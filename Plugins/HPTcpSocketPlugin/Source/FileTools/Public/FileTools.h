// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "IFileToolsBase.h"
#include "Modules/ModuleManager.h"

namespace FileTools
{
	enum EFileDialogFlags
	{
		None					= 0x00, // No flags
        Multiple				= 0x01  // Allow multiple file selections
    };
}


class FFileToolsModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;


	static IFileToolsBase* Get()
	{
		FFileToolsModule& FileToolsModule = FModuleManager::Get().LoadModuleChecked<FFileToolsModule>("FileTools");
		return FileToolsModule.GetSingleton();
	}

private:
	virtual IFileToolsBase* GetSingleton() const { return FileToolsBase; }
	
	IFileToolsBase* FileToolsBase = nullptr;
};
