// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileToosBlueprintFunctionLibrary.generated.h"


UENUM()
enum class EFileDialogFlag : uint8
{
	Single    = 0,
    Multiple
};


/**
 * 
 */
UCLASS()
class FILETOOLS_API UFileToosBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()


public:
	

	
	/**
	 * @Description 		: Opens the "open file" dialog for the platform
	 * @Author      		: byTangs
	 * @param WorldContextObject 
	 * @param DialogTitle	: The text for the title of the dialog window
	 * @param DefaultPath	: The path where the file dialog will open initially
	 * @param DefaultFile	: The file that the dialog will select initially
	 * @param FileTypes		: The type filters to show in the dialog. This string should be a "|" delimited list of (Description|Extensionlist) pairs. Extensionlists are ";" delimited.
	 * @param Flags			: Details about the dialog,Sig.multi
	 * @param OutFilenames	: The filenames that were selected in the dialog
	 * @return     			: true if files were successfully selected
	 */
	UFUNCTION(BlueprintCallable,meta=(WorldContext="WorldContextObject"),Category= "FileTools")
    static bool OpenFileDialog(const UObject* WorldContextObject, const FString& DialogTitle, const FString& DefaultPath, const FString& DefaultFile, const FString& FileTypes, EFileDialogFlag
                               Flags, TArray<FString>& OutFilenames);


	
	/**
	 * @Description 			: Opens the "save file" dialog for the platform
	 * @Author     			    : byTangs
	 * @param WorldContextObject
	 * @param DialogTitle		: The text for the title of the dialog window
	 * @param DefaultPath		: The path where the file dialog will open initially
	 * @param DefaultFile		: he file that the dialog will select initially
	 * @param FileTypes			: The type filters to show in the dialog. This string should be a "|" delimited list of (Description|Extensionlist) pairs. Extensionlists are ";" delimited.
	 * @param Flags				: Flags: Details about the dialog,Sig.multi
	 * @param OutFilenames		: The filenames that were selected in the dialog
	 * @return      			: true if files were successfully selected
	 */
	UFUNCTION(BlueprintCallable,meta=(WorldContext="WorldContextObject"),Category= "FileTools")
    static bool SaveFileDialog(const UObject* WorldContextObject, const FString& DialogTitle, const FString& DefaultPath, const FString& DefaultFile, const FString& FileTypes, EFileDialogFlag
                               Flags, TArray<FString>& OutFilenames);

	
	/**
	 * @Description 				: Opens the "choose folder" dialog for the platform
	 * @Author      				: byTangs
	 * @param WorldContextObject
	 * @param DialogTitle			: The text for the title of the dialog window
	 * @param DefaultPath			: The path where the file dialog will open initially
	 * @param OutFolderName			: The folder name that was selected in the dialog
	 * @return      				: true if folder choice was successfully selected
	 */
	UFUNCTION(BlueprintCallable,meta=(WorldContext="WorldContextObject"),Category= "FileTools")
    static bool OpenDirectoryDialog(const UObject* WorldContextObject, const FString& DialogTitle, const FString& DefaultPath, FString& OutFolderName);



	/**
	 * @Description 			: GetCleanName
	 * @Author      			: byTangs
	 * @param FilePath			: the full file path
	 * @param bWithExtension	: return file name with extension if true
	 * @return      			: the filename
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static FString GetCleanName(const FString& FilePath,bool bWithExtension = false);


	/**
	 * @Description 	: GetPath
	 * @Author      	: byTangs
	 * @param  FilePath	: file full path
	 * @return      	: the path in front of the filename
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static FString GetPath(const FString& FilePath);


	
	/**
	 * @Description  		: gets the extension for this filename.
	 * @Author       		: byTangs
	 * @param InPath  		: file name
	 * @param bIncludeDot	: IncludeDot
	 * @return      		: the extension of this filename, or an empty string if the filename doesn't have an extension.
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static FString GetExtension( const FString& InPath, bool bIncludeDot=false );

	
	/**
	 * @Description  : DirectoryExists
	 * @Author       : byTangs
	 * @param InPath :
	 * @return       : true if this directory was found, false otherwise 
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool DirectoryExists(const FString& InPath);

	
	/**
	 * @Description  : IsDrive
	 * @Author       : byTangs
	 * @param InPath :
	 * @return       : true if this path represents a root drive or volume 
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool IsDrive(const FString& InPath);



	
	/**
	 * @Description  : IsRelative
	 * @Author       : byTangs
	 * @param InPath :
	 * @return       : true if this path is relative to another path
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool IsRelative(const FString& InPath);


	
	/**
	 * @Description  : IsSamePath
	 * @Author       : byTangs
	 * @param  PathA : First path to check.
	 * @param  PathB : Second path to check.
	 * @return       : True if both paths are the same. False otherwise.
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool IsSamePath(const FString& PathA, const FString& PathB);


	/**
	 * @Description 		 : IsUnderDirectory
	 * @Author      		 : byTangs
	 * @param InPath			
	 * @param InDirectory		
	 * @return     			 : true if a path is under a given directory
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool IsUnderDirectory(const FString& InPath, const FString& InDirectory);


	
	/**
	 * @Description 	: Converts a relative path name to a fully qualified name relative to the process BaseDir().
	 * @Author      	: byTangs
	 * @param InPath	:
	 * @return      	
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static FString ConvertRelativePathTo_BaseDir(const FString& InPath);


	/**
	 * @Description 	: Converts a relative path name to a fully qualified name relative to the specified BasePath.
	 * @Author      	: byTangs
	 * @param BasePath	
	 * @param InPath	
	 * @return      	
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static FString ConvertRelativePathTo_BasePath(const FString& BasePath, const FString& InPath);


	
	/**
	 * @Description 	: FileExists
	 * @Author      	: byTangs
	 * @param Filename		
	 * @return      	: true if the file exists.
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool		FileExists(const FString& Filename);

	
	/**
	 * @Description 	: get file size
	 * @Author      	: byTangs
	 * @param Filename	
	 * @return      	: -1 if file not exists
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static int64	FileSize(const FString& Filename);


	/**
	 * @Description 	: IsReadOnly
	 * @Author      	: byTangs
	 * @param Filename 
	 * @return      	:  true if file is read only
	 */
	UFUNCTION(BlueprintCallable,BlueprintPure,Category= "FileTools")
	static bool		IsReadOnly(const FString& Filename);

	
	/**
	 * @Description 	: Delete a file 
	 * @Author      	: byTangs
	 * @param Filename	
	 * @return      	: true if the file exists. Will not delete read only files
	 */
	UFUNCTION(BlueprintCallable,Category= "FileTools")
	static bool		DeleteFile(const FString& Filename);


	
	/**
	 * @Description : Attempt to move a file. 
	 * @Author      : byTangs
	 * @param To	
	 * @param From	
	 * @return      : true if successful. Will not overwrite existing files.
	 */
	UFUNCTION(BlueprintCallable,Category= "FileTools")
	static  bool	MoveFile(const FString& To, const FString& From);


	
	/**
	 * @Description 				: Attempt to change the read only status of a file.
	 * @Author      				: byTangs
	 * @param Filename				
	 * @param bNewReadOnlyValue		
	 * @return     									 
	 */
	UFUNCTION(BlueprintCallable,Category= "FileTools")
	static 	bool	SetReadOnly(const FString& Filename, bool bNewReadOnlyValue);


	
	/**
	 * @Description 	: Create a directory and return true if the directory was created or already existed
	 * @Author      	: byTangs
	 * @param Directory 
	 * @return      
	 */
	UFUNCTION(BlueprintCallable,Category= "FileTools")
	static bool		CreateDirectory(const FString& Directory);

	
	/**
	 * @Description 	: Delete a directory and return true if the directory was deleted or otherwise does not exist
	 * @Author      	: byTangs
	 * @param Directory :
	 * @return      
	 */
	UFUNCTION(BlueprintCallable,Category= "FileTools")
	static bool		DeleteDirectory(const FString& Directory);


	UFUNCTION(BlueprintCallable,BlueprintPure,meta=(DisplayName = "ToString (int64)", CompactNodeTitle = "->", BlueprintAutocast), Category="FileTools")
	static FString CovertInt64ToString(const int64& Value);
	
};
