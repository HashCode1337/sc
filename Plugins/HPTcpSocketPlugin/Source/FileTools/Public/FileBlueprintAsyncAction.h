// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FileToosBlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "Async/AsyncWork.h"
#include "UObject/Stack.h"
#include "HAL/PlatformFilemanager.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Misc/FileHelper.h"
#include "Math/NumericLimits.h"
#include "FileBlueprintAsyncAction.generated.h"


DECLARE_DELEGATE(FOnAsyncTaskFinish)

class FFileCopyAsyncTask : public FNonAbandonableTask
{
public:
	
	FFileCopyAsyncTask(const FString& InFrom,const FString& InTo,const FOnAsyncTaskFinish& InOnFinish)
		:CopyFrom(InFrom)
		,CopyTo(InTo)
		,OnFinish(InOnFinish)
	{	
	}
	
	~FFileCopyAsyncTask()
	{
		FFrame::KismetExecutionMessage(TEXT("File Copy Async Task Finish"),ELogVerbosity::Warning);
	}


	void DoWork()
	{
		FPlatformFileManager::Get().GetPlatformFile().CopyFile(*CopyTo,*CopyFrom);
		OnFinish.ExecuteIfBound();
	}

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FFileAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}
	

private:

	/** file full path*/
	FString CopyFrom , CopyTo;

	/** Async Task Finish Delegate*/
	FOnAsyncTaskFinish OnFinish;
};



/**
 * 
 */

UCLASS()
class FILETOOLS_API UFileCopyBlueprintAsyncAction : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()


	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", Category = "FileTools"))
	static UFileCopyBlueprintAsyncAction* CopyFileAsync(const FString& InFrom, const FString& InTo);

	/** Called to trigger the action once the delegates have been bound */
	virtual void Activate()override;

private:	
	/** Called at completion  to execute delegate */
	virtual void ExecuteCompleted();

private:

	/** file full path*/
	FString CopyFrom , CopyTo;
	
	/** Called at Task completion */
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTaskFinish);
	UPROPERTY(BlueprintAssignable,meta = (AllowPrivateAccess = "True"))
	FTaskFinish Finish ;

	/** Async Task Finish Delegate*/
	FOnAsyncTaskFinish OnFinish;
};





class FFileMoveAsyncTask : public FNonAbandonableTask
{

public:
	FFileMoveAsyncTask(const FString& InFrom,const FString& InTo,const FOnAsyncTaskFinish& InOnFinish)
		:MoveFrom(InFrom)
        ,MoveTo(InTo)
        ,OnFinish(InOnFinish)
	{
		
	}

	~FFileMoveAsyncTask()
	{
		FFrame::KismetExecutionMessage(TEXT("File Move Async Task Finish"),ELogVerbosity::Warning);
	}

	void DoWork()
	{
		FPlatformFileManager::Get().GetPlatformFile().MoveFile(*MoveTo,*MoveFrom);
		OnFinish.ExecuteIfBound();
	}

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FFileMoveAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}
	
private:
	/** file full path*/
	FString MoveFrom , MoveTo;

	/** Async Task Finish Delegate*/
	FOnAsyncTaskFinish OnFinish;
};



UCLASS()
class FILETOOLS_API UFileMoveBlueprintAsyncAction : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()


	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", Category = "FileTools"))
    static UFileMoveBlueprintAsyncAction* MoveFileAsync(const FString& InFrom, const FString& InTo);

	/** Called to trigger the action once the delegates have been bound */
	virtual void Activate()override;

private:	
	/** Called at completion  to execute delegate */
	virtual void ExecuteCompleted();

private: 

	/** file full path*/
	FString MoveFrom , MoveTo;
	 
	/** Called at Task completion */
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTaskFinish);
	UPROPERTY(BlueprintAssignable,meta = (AllowPrivateAccess = "True"))
	FTaskFinish Finish ;

	/** Async Task Finish Delegate*/
	FOnAsyncTaskFinish OnFinish;
};



DECLARE_DELEGATE_TwoParams(FOnLoadFileAsyncTaskFinish,const TArray<uint8>&,bool)

class FLoadFileTask : public  FNonAbandonableTask
{
public:

	
	/**
	 * @brief            			    : async load file to array
	 * @author           	            : byTangs
	 * @param InFileName			    : file path
	 * @param InAsyncTaskFinishDelegate : delegate called at load finish 
	 * @param OutBytes				    : loaded file raw data
	 * @return  
	 */
	FLoadFileTask(const FString& InFileName,const FOnLoadFileAsyncTaskFinish& InAsyncTaskFinishDelegate,TArray<uint8>& OutBytes)
		:Filename(InFileName)
		,OnFinish(InAsyncTaskFinishDelegate)
		,FileRawData(OutBytes)
	{
		
	}


	void DoWork()
	{
		if(Filename.IsEmpty() || !UFileToosBlueprintFunctionLibrary::FileExists(Filename))
		{
			
			OnFinish.ExecuteIfBound(FileRawData,false);
			return;
		}

		if(UFileToosBlueprintFunctionLibrary::FileSize(Filename) > MAX_int32)
		{
			OnFinish.ExecuteIfBound(FileRawData,false);
			return;
		}
		
		const bool LoadResult = FFileHelper::LoadFileToArray(FileRawData,*Filename);
		OnFinish.ExecuteIfBound(FileRawData,LoadResult);
	}


	
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FLoadFileTask, STATGROUP_ThreadPoolAsyncTasks);
	}


private:

	/** file path */
	FString Filename;
	
	/** Async Task Finish Delegate*/
	FOnLoadFileAsyncTaskFinish OnFinish;

	/** file data */
	TArray<uint8>& FileRawData;
};






UCLASS()
class FILETOOLS_API ULoadFileToArrayBlueprintAsyncAction : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

	
	
	/**
	 * @brief            : Load file to array ,note :do not load huge file
	 * @author           : byTangs
	 * @param InFileName :  
	 * @return           
	 */
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", Category = "FileTools"))
    static ULoadFileToArrayBlueprintAsyncAction* LoadFileToArrayAsync(const FString& InFileName);

	
	/** Called to trigger the action once the delegates have been bound */
	virtual void Activate()override;

private:	
	/** Called at completion  to execute delegate */
	virtual void ExecuteCompleted(const TArray<uint8>& OutFileBytes,bool bSuccess);
	
private:

	/** file path*/
	FString		   FileName;

	/** out bytes*/
	TArray<uint8>  ResultTemp;

	
	/** Called at Task completion */
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTaskFinish,const TArray<uint8>&,Result,bool,Success);
	UPROPERTY(BlueprintAssignable,meta = (AllowPrivateAccess = "True"))
	FTaskFinish Finish ;


	/** finish delegate async task */
	FOnLoadFileAsyncTaskFinish OnFinishDelegate;
};
