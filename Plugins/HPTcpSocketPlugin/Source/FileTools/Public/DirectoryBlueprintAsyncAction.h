// Copyright 2020 byTangs, All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "FileToosBlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "Async/AsyncWork.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "HAL/PlatformFilemanager.h"
#include "DirectoryBlueprintAsyncAction.generated.h"





DECLARE_DELEGATE_TwoParams(FOnVisitDelegate,const FString&,bool)

DECLARE_DELEGATE(FOnRecursivelyFinishDelegate)

class FDirectoryRecursivelyTask : public FNonAbandonableTask , public IPlatformFile::FDirectoryVisitor
{
public:
	FDirectoryRecursivelyTask(const FString& InDirectory,FString& InCurrentRecursivelyDirectory,
		const FOnVisitDelegate& InVisitDelegate,const FOnRecursivelyFinishDelegate& InRecursivelyFinishDelegate)
		:Directory(InDirectory)
		,CurrentRecursivelyDirectory(InCurrentRecursivelyDirectory)
		,OnVisitDelegate(InVisitDelegate)
		,OnRecursivelyFinishDelegate(InRecursivelyFinishDelegate)
	{
		
	}
	
	~FDirectoryRecursivelyTask()
	{
		FFrame::KismetExecutionMessage(TEXT("Directory Recursively Task Finish"),ELogVerbosity::Warning);
	}

	void DoWork()
	{

		if(!UFileToosBlueprintFunctionLibrary::DirectoryExists(Directory))
		{
			OnRecursivelyFinishDelegate.ExecuteIfBound();
			return;
		}
		
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
		PlatformFile.IterateDirectoryRecursively(*Directory,*this);
		OnRecursivelyFinishDelegate.ExecuteIfBound();
	}


	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FDirectoryRecursivelyTask, STATGROUP_ThreadPoolAsyncTasks);
	}


	virtual bool Visit(const TCHAR* FilenameOrDirectory, bool bIsDirectory) override
	{
		CurrentRecursivelyDirectory = FString(FilenameOrDirectory);
		OnVisitDelegate.ExecuteIfBound(CurrentRecursivelyDirectory,bIsDirectory);
		return true;
	}

	
private:

	FString  Directory;
	
	/** Current Recursively Directory */
	FString& CurrentRecursivelyDirectory;

	/**  On Visit delegate*/
	FOnVisitDelegate OnVisitDelegate;

	/** Recursively directory finish delegate*/
	FOnRecursivelyFinishDelegate OnRecursivelyFinishDelegate ;
};


/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFinish);

UCLASS()
class FILETOOLS_API UDirectoryBlueprintAsyncAction : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	
	/**
	 * @Description 			: IterateDirectoryRecursively
	 * @Author      			: byTangs
	 * @param InStartDirectory    
	 * @return   
	 */
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", Category = "FileTools"))
	static UDirectoryBlueprintAsyncAction* IterateDirectoryRecursively(const FString& InStartDirectory);

	/** Called to trigger the action once the delegates have been bound */
	virtual void Activate()override;

private:
	
	/** Called at completion  to execute delegate */
	virtual void ExecuteCompleted();

	
	virtual void VisitFunction(const FString& OutDirectoryOrFilename,bool IsDirectory);
	
private:

	/** Directory To Recursively*/
	FString 					StartDirectory;

	FString  					CurrentRecursivelyDirectory;                    
	
	/** called when visit directory*/
	FOnVisitDelegate 			VisitDelegate;

	/** Called at Task completion */
	FOnRecursivelyFinishDelegate RecursivelyFinishDelegate;

	/** called when visit directory */
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnVisit,const FString,DirectoryOrFilename,bool,bIsDirectory);
	UPROPERTY(BlueprintAssignable,meta = (AllowPrivateAccess = "True"))
	FOnVisit Visit;
	
	/** Called at completion */
	UPROPERTY(BlueprintAssignable,meta = (AllowPrivateAccess = "True"))
	FOnFinish Finish;
};
