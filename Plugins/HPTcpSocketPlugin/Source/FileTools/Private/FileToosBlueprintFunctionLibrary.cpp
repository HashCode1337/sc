// Copyright 2020 byTangs, All Rights Reserved.


#include "FileToosBlueprintFunctionLibrary.h"

#include "FileTools.h"
#include "Engine/World.h"
#include "Engine/GameViewportClient.h"
#include "Widgets/SWindow.h"
#include "GenericPlatform/GenericWindow.h"
#include "IFileToolsBase.h"
#include "Misc/Paths.h"

bool UFileToosBlueprintFunctionLibrary::OpenFileDialog(const UObject* WorldContextObject, const FString& DialogTitle,
    const FString& DefaultPath, const FString& DefaultFile, const FString& FileTypes, EFileDialogFlag Flags,
    TArray<FString>& OutFilenames)
{
    void* ParentWindowsHandle = nullptr;

    if(IsValid(WorldContextObject))
    {
        ParentWindowsHandle = WorldContextObject->GetWorld()->GetGameViewport()->GetWindow().Get()->GetNativeWindow().Get()->GetOSWindowHandle();
    }

    return FFileToolsModule::Get()->OpenFileDialog(ParentWindowsHandle,DialogTitle,DefaultPath,DefaultFile,FileTypes,static_cast<uint32>(Flags),OutFilenames);
}

bool UFileToosBlueprintFunctionLibrary::SaveFileDialog(const UObject* WorldContextObject, const FString& DialogTitle,
    const FString& DefaultPath, const FString& DefaultFile, const FString& FileTypes, EFileDialogFlag Flags,
    TArray<FString>& OutFilenames)
{
    void* ParentWindowsHandle = nullptr;

    if(IsValid(WorldContextObject))
    {
        ParentWindowsHandle = WorldContextObject->GetWorld()->GetGameViewport()->GetWindow().Get()->GetNativeWindow().Get()->GetOSWindowHandle();
    }
    return FFileToolsModule::Get()->SaveFileDialog(ParentWindowsHandle,DialogTitle,DefaultPath,DefaultFile,FileTypes,static_cast<uint32>(Flags),OutFilenames);
}

bool UFileToosBlueprintFunctionLibrary::OpenDirectoryDialog(const UObject* WorldContextObject,
    const FString& DialogTitle, const FString& DefaultPath, FString& OutFolderName)
{
    void* ParentWindowsHandle = nullptr;

    if(IsValid(WorldContextObject))
    {
        ParentWindowsHandle = WorldContextObject->GetWorld()->GetGameViewport()->GetWindow().Get()->GetNativeWindow().Get()->GetOSWindowHandle();
    }
    return FFileToolsModule::Get()->OpenDirectoryDialog(ParentWindowsHandle,DialogTitle,DefaultPath,OutFolderName);
}

FString UFileToosBlueprintFunctionLibrary::GetCleanName(const FString& FilePath, bool bWithExtension)
{
    return bWithExtension ? FPaths::GetCleanFilename(FilePath) : FPaths::GetBaseFilename(FilePath);
}

FString UFileToosBlueprintFunctionLibrary::GetPath(const FString& FilePath)
{
    return  FPaths::GetPath(FilePath);
}

FString UFileToosBlueprintFunctionLibrary::GetExtension(const FString& InPath, bool bIncludeDot)
{
    return  FPaths::GetExtension(InPath,bIncludeDot);
}

bool UFileToosBlueprintFunctionLibrary::DirectoryExists(const FString& InPath)
{
    return  FPaths::DirectoryExists(InPath);
}

bool UFileToosBlueprintFunctionLibrary::IsDrive(const FString& InPath)
{
    return  FPaths::IsDrive(InPath);
}

bool UFileToosBlueprintFunctionLibrary::IsRelative(const FString& InPath)
{
    return  FPaths::IsRelative(InPath);
}

bool UFileToosBlueprintFunctionLibrary::IsSamePath(const FString& PathA, const FString& PathB)
{
    return  FPaths::IsSamePath(PathA,PathB);
}

bool UFileToosBlueprintFunctionLibrary::IsUnderDirectory(const FString& InPath, const FString& InDirectory)
{
    return  FPaths::IsUnderDirectory(InPath,InDirectory);
}

FString UFileToosBlueprintFunctionLibrary::ConvertRelativePathTo_BaseDir(const FString& InPath)
{
    return  FPaths::ConvertRelativePathToFull(InPath);
}

FString UFileToosBlueprintFunctionLibrary::ConvertRelativePathTo_BasePath(const FString& BasePath, const FString& InPath)
{
    return  FPaths::ConvertRelativePathToFull(BasePath,InPath);
}

bool UFileToosBlueprintFunctionLibrary::FileExists(const FString& Filename)
{
   return Filename.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().FileExists(*Filename);
}


int64 UFileToosBlueprintFunctionLibrary::FileSize(const FString& Filename)
{
    return Filename.IsEmpty() ? -1 : FPlatformFileManager::Get().GetPlatformFile().FileSize(*Filename);
}

bool UFileToosBlueprintFunctionLibrary::IsReadOnly(const FString& Filename)
{
    return Filename.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().IsReadOnly(*Filename);
}

bool UFileToosBlueprintFunctionLibrary::DeleteFile(const FString& Filename)
{
    return Filename.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*Filename);
}

bool UFileToosBlueprintFunctionLibrary::MoveFile(const FString& To, const FString& From)
{
    if(To.IsEmpty() || From.IsEmpty()) return false;
    return FPlatformFileManager::Get().GetPlatformFile().MoveFile(*To,*From);
}

bool UFileToosBlueprintFunctionLibrary::SetReadOnly(const FString& Filename, bool bNewReadOnlyValue)
{
    return Filename.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().SetReadOnly(*Filename,bNewReadOnlyValue);
}

bool UFileToosBlueprintFunctionLibrary::CreateDirectory(const FString& Directory)
{
    return Directory.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().CreateDirectory(*Directory);
}

bool UFileToosBlueprintFunctionLibrary::DeleteDirectory(const FString& Directory)
{
    return Directory.IsEmpty() ? false : FPlatformFileManager::Get().GetPlatformFile().DeleteDirectory(*Directory);
}

FString UFileToosBlueprintFunctionLibrary::CovertInt64ToString(const int64& Value)
{
    return FString::Printf(TEXT("%lld"),Value);
}
