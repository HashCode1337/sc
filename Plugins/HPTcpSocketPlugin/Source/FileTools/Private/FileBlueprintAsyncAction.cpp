// Copyright 2020 byTangs, All Rights Reserved.

#include "FileBlueprintAsyncAction.h"
#include "Async/TaskGraphInterfaces.h"
#include "Async/Async.h"

UFileCopyBlueprintAsyncAction* UFileCopyBlueprintAsyncAction::CopyFileAsync(const FString& InFrom, const FString& InTo)
{
    UFileCopyBlueprintAsyncAction* Action = NewObject<UFileCopyBlueprintAsyncAction>();
    Action->CopyFrom = InFrom;
    Action->CopyTo   = InTo;
    Action->OnFinish.BindUObject(Action,&UFileCopyBlueprintAsyncAction::ExecuteCompleted);
    return Action;
}

void UFileCopyBlueprintAsyncAction::Activate()
{
    (new FAutoDeleteAsyncTask<FFileCopyAsyncTask>(CopyFrom,CopyTo,OnFinish))->StartBackgroundTask();
}

void UFileCopyBlueprintAsyncAction::ExecuteCompleted()
{
    AsyncTask(ENamedThreads::GameThread,[this]()
    {
        this->Finish.Broadcast();
    });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    SetReadyToDestroy();
}

UFileMoveBlueprintAsyncAction* UFileMoveBlueprintAsyncAction::MoveFileAsync(const FString& InFrom, const FString& InTo)
{
    UFileMoveBlueprintAsyncAction* Action = NewObject<UFileMoveBlueprintAsyncAction>();
    Action->MoveFrom    = InFrom;
    Action->MoveTo      = InTo;
    Action->OnFinish.BindUObject(Action,&UFileMoveBlueprintAsyncAction::ExecuteCompleted);
    return Action;
}

void UFileMoveBlueprintAsyncAction::Activate()
{
    (new FAutoDeleteAsyncTask<FFileMoveAsyncTask>(MoveFrom,MoveTo,OnFinish))->StartBackgroundTask();
}

void UFileMoveBlueprintAsyncAction::ExecuteCompleted()
{
    AsyncTask(ENamedThreads::GameThread,[this]()
    {
        this->Finish.Broadcast();
    });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    SetReadyToDestroy();
}

ULoadFileToArrayBlueprintAsyncAction* ULoadFileToArrayBlueprintAsyncAction::LoadFileToArrayAsync(
    const FString& InFileName)
{
    ULoadFileToArrayBlueprintAsyncAction* Action = NewObject<ULoadFileToArrayBlueprintAsyncAction>();
    Action->FileName   = InFileName;
    Action->OnFinishDelegate.BindUObject(Action,&ULoadFileToArrayBlueprintAsyncAction::ExecuteCompleted);
    return Action;
}

void ULoadFileToArrayBlueprintAsyncAction::Activate()
{
    (new FAutoDeleteAsyncTask<FLoadFileTask>(FileName,OnFinishDelegate,ResultTemp))->StartBackgroundTask();
}

void ULoadFileToArrayBlueprintAsyncAction::ExecuteCompleted(const TArray<uint8>& OutFileBytes,bool bSuccess)
{
   AsyncTask(ENamedThreads::GameThread,[this,&OutFileBytes,bSuccess]()
   {
       this->Finish.Broadcast(OutFileBytes,bSuccess);
   });   
   SetReadyToDestroy();
}


