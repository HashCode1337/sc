// Copyright 2020 byTangs, All Rights Reserved.


#include "DirectoryBlueprintAsyncAction.h"
#include "Async/TaskGraphInterfaces.h"
#include "Async/Async.h"

UDirectoryBlueprintAsyncAction* UDirectoryBlueprintAsyncAction::IterateDirectoryRecursively(
    const FString& InStartDirectory)
{
    UDirectoryBlueprintAsyncAction* Action = NewObject<UDirectoryBlueprintAsyncAction>();
    Action->StartDirectory = InStartDirectory;
    Action->VisitDelegate.BindUObject(Action,&UDirectoryBlueprintAsyncAction::VisitFunction);
    Action->RecursivelyFinishDelegate.BindUObject(Action,&UDirectoryBlueprintAsyncAction::ExecuteCompleted);
    return Action;
}

void UDirectoryBlueprintAsyncAction::Activate()
{
    (new FAutoDeleteAsyncTask<FDirectoryRecursivelyTask>(StartDirectory,CurrentRecursivelyDirectory,VisitDelegate,RecursivelyFinishDelegate))->StartBackgroundTask();
}

void UDirectoryBlueprintAsyncAction::ExecuteCompleted()
{
    AsyncTask(ENamedThreads::GameThread,[this]()
    {
        this->Finish.Broadcast();
    });
    SetReadyToDestroy();
}

void UDirectoryBlueprintAsyncAction::VisitFunction(const FString& OutDirectoryOrFilename, bool IsDirectory)
{
    FString DirectoryOrNameTemp = OutDirectoryOrFilename;
    AsyncTask(ENamedThreads::GameThread,[this,DirectoryOrNameTemp,IsDirectory]()
    {
        this->Visit.Broadcast(DirectoryOrNameTemp,IsDirectory);
    });
}
