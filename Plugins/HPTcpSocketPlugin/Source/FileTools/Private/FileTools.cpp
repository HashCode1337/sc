// Copyright 2020 byTangs, All Rights Reserved.

#include "FileTools.h"

#include "Windows/WindowsFileTools.h"

#define LOCTEXT_NAMESPACE "FFileToolsModule"

void FFileToolsModule::StartupModule()
{
	FileToolsBase = new FFileTools();
}

void FFileToolsModule::ShutdownModule()
{
	if(FileToolsBase != nullptr)
	{
		delete FileToolsBase;
		FileToolsBase = nullptr;
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FFileToolsModule, FileTools)