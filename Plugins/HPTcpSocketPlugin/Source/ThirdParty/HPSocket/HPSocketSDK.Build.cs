// Copyright 2020 byTangs, All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;

public class HPSocketSDK : ModuleRules
{
	public HPSocketSDK(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;

		if(Target.Platform == UnrealTargetPlatform.Win64)
        {
			string HPSDKIncPath = Path.GetFullPath(ModuleDirectory) + "/include/";

			string LibName = "HPSocket_U";

			string HPSDKLibPath = Path.GetFullPath(ModuleDirectory) + "/lib/";

			PublicSystemIncludePaths.Add(HPSDKIncPath);

			PublicAdditionalLibraries.Add(Path.Combine(HPSDKLibPath, LibName + ".lib"));

			string DLLName = LibName + ".dll";
			string DllPath = Path.GetFullPath(Path.Combine(ModuleDirectory,"../../../Binaries/ThirdParty/HPSocket/"));
			PublicDelayLoadDLLs.Add(DLLName);
			
			RuntimeDependencies.Add(Path.Combine(DllPath,DLLName));
			
		}
	}
}
