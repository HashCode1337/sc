// Copyright 2020 byTangs, All Rights Reserved.
#pragma once
#include "CoreMinimal.h"
#include "HPTcpSocketCoreMinimal.h"


/** GetHostName */
HPTCPSOCKETPLUGIN_API  bool GetHostNameInternal(FString& OutHostName);

/** GetLocalAddress */
HPTCPSOCKETPLUGIN_API  bool GetLocalAddressInternal(const EIPAddressType& IPAddressType, TArray<FString>& OutAddress);

/** check ip address is valid */
HPTCPSOCKETPLUGIN_API  bool IPIsValidInternal(const FString& InAddress);
