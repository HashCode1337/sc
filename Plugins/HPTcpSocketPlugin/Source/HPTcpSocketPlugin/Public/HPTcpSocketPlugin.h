// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FHPTcpSocketPluginModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;


private:

	/**
	 * @brief   : Delay Load HPSocket DLLs
	 * @author  : byTangs
	 * @return  : bool
	 */
	bool LoadHPSocketDll();



	/**
	 * @brief   : Release HPSocket Dll
	 * @author  : byTangs
	 * @return  : void
	 */
	void ReleaseHPSocketDll();

private:

	/** HpSocket Dll Handle */
	void* HpSocketHandle = nullptr;
};
