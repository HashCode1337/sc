// Copyright 2020 byTangs, All Rights Reserved.

#pragma once
#include  "CoreMinimal.h"


enum class EIPAddressType : uint8
{
    ALL = 0,
    IPV4,
    IPV6
};


DECLARE_LOG_CATEGORY_EXTERN(HPLog, Log, All)

#define HP_LOG(Verbosity,Format,...) \
        UE_LOG(HPLog,Verbosity,Format,##__VA_ARGS__)



