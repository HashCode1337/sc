// Copyright 2020 byTangs, All Rights Reserved.

#include "HPTcpSocketPlugin.h"
#include "Interfaces/IPluginManager.h"
#include "Misc/MessageDialog.h"
#include "Windows/WindowsPlatformProcess.h"
#include "Misc/Paths.h"

#define LOCTEXT_NAMESPACE "FHPTcpSocketPluginModule"


DEFINE_LOG_CATEGORY(HPLog);



void FHPTcpSocketPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
    if (!LoadHPSocketDll())
    {
        HP_LOG(Error, TEXT("Can Not Load HPSocket DLL..."));
        //FGenericPlatformMisc::RequestExit(false);
    }
}

void FHPTcpSocketPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
    ReleaseHPSocketDll();
}



bool FHPTcpSocketPluginModule::LoadHPSocketDll()
{
    FString HPSocketDllName;

    TSharedPtr<IPlugin> HPPlugin = IPluginManager::Get().FindPlugin(TEXT("HPTcpSocketPlugin"));

    if (!HPPlugin.IsValid())
    {
        return false;
    }

    HPSocketDllName = FPaths::ConvertRelativePathToFull(HPPlugin.Get()->GetBaseDir()) / TEXT("Binaries/ThirdParty/HPSocket/HPSocket_U.dll");

    HP_LOG(Warning, TEXT("Dll Path Is %s"), *HPSocketDllName);

    if (!FPaths::FileExists(HPSocketDllName))
    {
        FText ErrorTitle = FText::FromString(TEXT("Error"));
        FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(TEXT("Can Not Found HPSocket Dll")), &ErrorTitle);
        return false;
    }

    HpSocketHandle = FPlatformProcess::GetDllHandle(*HPSocketDllName);
    return HpSocketHandle != nullptr;
}

void FHPTcpSocketPluginModule::ReleaseHPSocketDll()
{
    if (HpSocketHandle != nullptr)
    {
        FPlatformProcess::FreeDllHandle(HpSocketHandle);
        HpSocketHandle = nullptr;
    }
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FHPTcpSocketPluginModule, HPTcpSocketPlugin)