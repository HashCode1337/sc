// Copyright 2020 byTangs, All Rights Reserved.


#include "HPTcpClientManager.h"
#include "HpSocketUtilities.h"
#include "Async/TaskGraphInterfaces.h"
#include "Async/Async.h"
#include "Kismet/KismetMathLibrary.h"
#include <string>
#include "HPTcpClientSettings.h"

UHPTcpClientManager::~UHPTcpClientManager()
{
    HP_LOG(Warning, TEXT("Destory Tcp Client Manager ... "));
}


bool UHPTcpClientManager::ConnectToServer(const FString& Ip, const int32 Port, const FTcpClientStartOptions& Options)
{

	if(!IPIsValidInternal(Ip))
	{
		return false;
	}

	
    if (!TcpClientListener.IsValid())
    {
		TcpClientListener = MakeShareable(new FTcpClientListener());

		if (!TcpClientListener.IsValid())
			return false;
    }

	BindClientDelegate(Options);
	ApplyTcpClientSettings();

    return  TcpClientListener.Get()->GetTcpClient()->Start(*Ip, Port);
}

void UHPTcpClientManager::CloseTcpClient()
{
	if(TcpClientListener.IsValid())
	{
		AsyncTask(ENamedThreads::GameThread,[this]()
        {
            if(this->HasStarted())
            {
                this->TcpClientListener.Get()->GetTcpClient()->Stop();
            }
        });      
	}

}

bool UHPTcpClientManager::SendMessage(const FString& Message)
{
    if (!TcpClientListener.IsValid() || Message.IsEmpty()) return false;

    // Data Size
    const FString DataLength   = FString::FromInt(Message.Len() + 1);

    // send data type (default type : message,RawBytes,File)
    const FString SendDataType = TEXT("Message");

    // the name of file, only valid in file type
    const FString SendDataName = TEXT("NULL");

    // first send message info ,send send real data
    const FString SendDataHead = DataLength + TEXT(":") + SendDataType + TEXT(":") + SendDataName;
    
    const std::string DataHeadInfo = TCHAR_TO_UTF8(*SendDataHead);

    const std::string RealData     = TCHAR_TO_UTF8(*Message);

    const int32 DataRealSize = Message.Len() + 1;
    
    if(DataRealSize > GetMaxPackSize())
    {
        if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
            return false;
        }
        
    	int64 RemainingSizeToSent = DataRealSize;
    	while(RemainingSizeToSent > 0 )
    	{
    		const int64 SizeToSent = FMath::Min(GetMaxPackSize(),RemainingSizeToSent);
    		if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(RealData.c_str() + SizeToSent),SizeToSent))
    		{
    			HP_LOG(Error,TEXT("Send Message To Server Failed , Error Code = %ul"),SYS_GetLastError());
    			return false;
    		}   			
    		RemainingSizeToSent -= SizeToSent;
    	}
        return true;
    }else
    {
        if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
        	HP_LOG(Error,TEXT("Send Message To Server Failed , Error Code = %ul"),SYS_GetLastError());
        	return false;
		}
        return TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(RealData.c_str()), RealData.length() + 1);
    }
}

bool UHPTcpClientManager::SendBytes(const TArray<uint8>& Bytes)
{
	if(Bytes.Num() <= 0) return false;


	const int32 DataRealSize = Bytes.Num();
    
	// Data Size
	const FString DataLength   = FString::FromInt(DataRealSize);

	// send data type (default type : message,RawBytes,File)
	const FString SendDataType = TEXT("Bytes");

	// the name of file, only valid in file type
	const FString SendDataName = TEXT("NULL");

	// first send message info ,send send real data
	const FString SendDataHead = DataLength + TEXT(":") + SendDataType + TEXT(":") + SendDataName;
    
	const std::string DataHeadInfo = TCHAR_TO_UTF8(*SendDataHead);

    
	if(DataRealSize > GetMaxPackSize())
	{
		if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
		{
			HP_LOG(Error,TEXT("Send Bytes To Server Failed , Error Code = %ul"),SYS_GetLastError());
			return false;
		}
        
		int64 RemainingSizeToSent = DataRealSize;
		while(RemainingSizeToSent > 0 )
		{
			const int64 SizeToSent = FMath::Min(GetMaxPackSize(),RemainingSizeToSent);
			if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(Bytes.GetData() + SizeToSent),SizeToSent))
			{
				HP_LOG(Error,TEXT("Send Bytes To Server Failed , Error Code = %ul"),SYS_GetLastError());
				return false;
			}
			RemainingSizeToSent -= SizeToSent;
		}
		return true;
	}else
	{
		if(!TcpClientListener.Get()->GetTcpClient()->Send(reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
		{
			HP_LOG(Error,TEXT("Send Bytes To Server Failed , Error Code = %ul"),SYS_GetLastError());
			return false;
		}
		return TcpClientListener.Get()->GetTcpClient()->Send(Bytes.GetData(), DataRealSize);
	}
}

void UHPTcpClientManager::SetMaxPackSize(const int64& NewMaxSize)
{
	if(TcpClientListener.IsValid() && NewMaxSize >= 131072 )
	{
		const DWORD MaxSize = UKismetMathLibrary::Clamp(NewMaxSize,131072,4194303);
		TcpClientListener.Get()->GetTcpClient()->SetMaxPackSize(MaxSize);
	}
}


void UHPTcpClientManager::SetKeepAliveTime(const int64& KeepAliveTime)
{
	if(TcpClientListener.IsValid())
	{
		const DWORD Time = UKismetMathLibrary::Clamp(KeepAliveTime,0,KeepAliveTime);
		TcpClientListener.Get()->GetTcpClient()->SetKeepAliveTime(Time);
	}
}

void UHPTcpClientManager::SetKeepAliveTimeInterval(const int64& KeepAliveInterval)
{
	if(TcpClientListener.IsValid())
	{
		const DWORD Time = UKismetMathLibrary::Clamp(KeepAliveInterval,0,KeepAliveInterval);
		TcpClientListener.Get()->GetTcpClient()->SetKeepAliveInterval(Time);
	}
}

bool UHPTcpClientManager::IsConnected() const
{
	if (!TcpClientListener.IsValid()) return false;
	return TcpClientListener.Get()->GetTcpClient()->IsConnected();
}

bool UHPTcpClientManager::HasStarted() const
{
	if (!TcpClientListener.IsValid()) return false;
	return TcpClientListener.Get()->GetTcpClient()->HasStarted();
}

int64 UHPTcpClientManager::GetMaxPackSize() const
{
	if (!TcpClientListener.IsValid() || !TcpClientListener.Get()->GetTcpClient()->HasStarted())
		return -1;
	return TcpClientListener.Get()->GetTcpClient()->GetMaxPackSize();
}

int64 UHPTcpClientManager::GetKeepAliveTime() const
{
	if (!TcpClientListener.IsValid())
		return -1;
	return TcpClientListener.Get()->GetTcpClient()->GetKeepAliveTime();
}

int64 UHPTcpClientManager::GetKeepAliveTimeInterval() const
{
	if (!TcpClientListener.IsValid())
		return -1;
	return TcpClientListener.Get()->GetTcpClient()->GetKeepAliveInterval();
}

int64 UHPTcpClientManager::GetConnectionID() const
{
	if (!TcpClientListener.IsValid() || !IsConnected())
		return  -1;
	return TcpClientListener.Get()->GetTcpClient()->GetConnectionID();
}

ETcpClientState UHPTcpClientManager::GetTcpClientState() const
{
	if(!TcpClientListener.IsValid())
		return ETcpClientState::NOTREADY;

	EnServiceState ServiceState = TcpClientListener.Get()->GetTcpClient()->GetState();
	return static_cast<ETcpClientState>(ServiceState);
}

bool UHPTcpClientManager::GetLocalAddress(FString& IPAddress, bool AppendPort) const
{
	if(!TcpClientListener.IsValid())
		return false;

	TCHAR OutAddress[50];
	int32 AddressLength = 50;
	uint16 OutPort = 0;
	if(TcpClientListener.Get()->GetTcpClient()->GetLocalAddress(OutAddress,AddressLength,OutPort))
	{
		IPAddress = AppendPort ? FString(OutAddress) + TEXT(":") + FString::FromInt(OutPort) : FString(OutAddress);
		return true;
	}
    
	return false;
}

bool UHPTcpClientManager::GetRemoteAddress(FString& RemoteHost, bool AppendPort) const
{
	if(!TcpClientListener.IsValid())
		return false;

	TCHAR OutAddress[50];
	int32 AddressLength = 50;
	uint16 OutPort = 0;
	if(TcpClientListener.Get()->GetTcpClient()->GetRemoteHost(OutAddress,AddressLength,OutPort))
	{
		RemoteHost = AppendPort ? FString(OutAddress) + TEXT(":") + FString::FromInt(OutPort) : FString(OutAddress);
		return true;
	}
    
	return false;
}

bool UHPTcpClientManager::GetPendingDataLength(int32& Pending) const
{
	if(!TcpClientListener.IsValid())
		return false;

	return TcpClientListener.Get()->GetTcpClient()->GetPendingDataLength(Pending);
}

void UHPTcpClientManager::BindClientDelegate(const FTcpClientStartOptions &Options)
{
	if (TcpClientListener.IsValid())
	{
		TcpClientStartOptions = MoveTemp(const_cast<FTcpClientStartOptions&>(Options));

		TcpClientListener.Get()->OnConnectedDelegate().BindWeakLambda(this,[this](const int64 ConnID)
		{
			AsyncTask(ENamedThreads::GameThread, [this, ConnID] { this->TcpClientStartOptions.OnConnected.ExecuteIfBound(ConnID); });
		});


		TcpClientListener.Get()->OnReceiveDelegate().BindWeakLambda(this,[this](const FString Message)
		{
			AsyncTask(ENamedThreads::GameThread, [this, Message] { this->TcpClientStartOptions.OnReceiveMessage.ExecuteIfBound(Message); });
		});


		TcpClientListener.Get()->OnCloseDelegate().BindWeakLambda(this,[this](const int64 ConnID, const int32 ErrorCode)
		{
			AsyncTask(ENamedThreads::GameThread, [this, ConnID, ErrorCode] { this->TcpClientStartOptions.OnClose.ExecuteIfBound(ConnID, ErrorCode); });
		});

		TcpClientListener.Get()->OnClientReceiveBytesDelegate().BindWeakLambda(this,[this](const int64 ConnID,const TArray<uint8>& Bytes)
		{
			AsyncTask(ENamedThreads::GameThread, [this, ConnID, Bytes] { this->TcpClientStartOptions.OnReceiveBytes.ExecuteIfBound(ConnID, Bytes); });
		});

	}	
}

void UHPTcpClientManager::ApplyTcpClientSettings()
{
	if (TcpClientListener.IsValid())
	{
		const UHPTcpClientSettings* TcpClientSettings = GetDefault<UHPTcpClientSettings>();
		if (TcpClientSettings != nullptr)
		{
			TcpClientListener.Get()->GetTcpClient()->SetKeepAliveInterval(TcpClientSettings->GetAbnormalAliveTimeInterval());

			TcpClientListener.Get()->GetTcpClient()->SetKeepAliveTime(TcpClientSettings->GetKeepAliveTimeInterval());

			TcpClientListener.Get()->GetTcpClient()->SetMaxPackSize(TcpClientSettings->GetMaxPackSize());

			TcpClientListener.Get()->GetTcpClient()->SetSocketBufferSize(TcpClientSettings->GetSocketBufferSize());
		}
	}
}
