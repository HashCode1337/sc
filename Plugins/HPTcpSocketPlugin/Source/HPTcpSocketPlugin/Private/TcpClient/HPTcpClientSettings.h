// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "Engine/EngineTypes.h"
#include "HPTcpClientSettings.generated.h"

/**
 * 
 */
UCLASS(Config = Game, defaultconfig)
class HPTCPSOCKETPLUGIN_API UHPTcpClientSettings : public UDeveloperSettings
{
	GENERATED_BODY()
	


public:

	UHPTcpClientSettings();


	/**
	 * @brief   : GetFileSavePath
	 * @author  : byTangs
	 */
	 FString GetFileSavePath()const;


	 /**
	  * @brief   : GetAbnormalAliveTimeInterval
	  * @author  : byTangs
	  */
	  int64 GetAbnormalAliveTimeInterval()const;


	  /**
	   * @brief   : GetKeepAliveTimeInterval
	   * @author  : byTangs
	   */
	   int64 GetKeepAliveTimeInterval()const;


	   /**
		 * @brief   : GetSocketBufferSize
		 * @author  : byTangs
		 */
	   int64 GetSocketBufferSize()const;


	   /**
		 * @brief   : GetMaxPackSize
		 * @author  : byTangs
		 */
	   int64 GetMaxPackSize()const;



#if WITH_EDITOR
	   /** Gets the section text, uses the classes DisplayName by default. */
	   virtual FText GetSectionText() const override;
#endif

	   virtual FName GetCategoryName() const override;
private:

	/** File Save Path When Receive File Form Client Or Server */
	UPROPERTY(Config, BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "Config")
	FDirectoryPath FileSavePath;


	/**
	 *  set keep alive time Interval(ms),default 20 * 1000
	 * 	If the heartbeat is not detected, the packet is considered disconnected,[Default: WinXP 5 times, Win7 10 times]
	 * 	Note : Do not send heartbeat packets if KeepAliveTime to be set 0
	 */
	UPROPERTY(Config, BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "Config")
	int64 AbnormalAliveTimeInterval;


	/** keep alive time(ms),default 60 *1000 . Do not send heartbeat packets if KeepAliveTime to be set 0 */
	UPROPERTY(Config, BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "Config")
	int64 KeepAliveTime;

	/** the communication data buffer size (adjusted for the average communication packet size, usually a multiple of 1024)*/
	UPROPERTY(Config, BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "Config")
	int64 MaxSocketBufferSize;


	/** Max Pack Size, size must under 4194303 and upper 131, 072, default size 262144.Note : Do Not Set 4194303 if no needed*/
	UPROPERTY(Config, BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "Config")
	int64 MaxPackSize;

};
