// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TcpClientListener.h"
#include "HPTcpClientManager.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnClientConneted, const int64, ConnectionID);

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnClientReceiveMessage,const FString, Message);

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnClientClose, const int64, ConnectionID, const int32, ErrorCode);

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnClientReceiveBytes, const int64, ConnectionID, const TArray<uint8>&,Bytes);

UENUM(BlueprintType,Blueprintable)
enum class ETcpClientState : uint8
{
	STARTING = 0  UMETA(DisplyName = "Starting"),
    STARTED		  UMETA(DisplyName = "Started"),
    STOPPING      UMETA(DisplyName = "Stopping"),
    STOPPED  	  UMETA(DisplyName = "Stopped"),
    NOTREADY      UMETA(DisplyName = "NotReady")
};



/**
 * Options specific to start tcp socket
 */
USTRUCT(BlueprintType)
struct FTcpClientStartOptions
{
	GENERATED_BODY()

	/** A callback to invoke when has new connection*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpClient")
	FOnClientConneted OnConnected;

	/** A callback to invoke when receive message */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpClient")
	FOnClientReceiveMessage OnReceiveMessage;

	/** A callback to invoke when receive Bytes */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpClient")
	FOnClientReceiveBytes  OnReceiveBytes;

	/** A callback to invoke when connection disconnection */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpClient")
	FOnClientClose OnClose;
};



/**
 * 
 */
UCLASS(BlueprintType)
class UHPTcpClientManager : public UObject
{
	GENERATED_BODY()

public:

	/**
	 * @brief   : Tcp Client Manager Constructor
	 * @author  : byTangs
	 * @returns : 
	 */
	 //UHPTcpClientManager();



	/**
	 * @brief   : Tcp Client Manager Destructor
	 * @author  : byTangs
	 * @returns : 
	 */
	 ~UHPTcpClientManager();

	
	/**
	 * @brief      : connect to tcp server
	 * @author	   : byTangs
	 * @param Ip   : Listen ip
	 * @param Port : Listen port
	 * @return     : true if success
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
	bool ConnectToServer(const FString& Ip, const int32 Port, const FTcpClientStartOptions& Options);


	/**
	 * @brief   : close tcp client
	 * @author  : byTangs
	 * @return  : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
	void CloseTcpClient();


	/**
	 * @brief				: send data to server
	 * @author				: byTangs
	 * @param Message		: data
	 * @return				: bool
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
	bool SendMessage(const FString& Message);


	/**
	* @brief   		   : Send Bytes To Connection
	* @author  		   : byTangs  
	* @param Bytes 	   :  
	* @return          : true if success
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
    bool SendBytes(const TArray<uint8>& Bytes);
	
	/**
	* @brief   		 : Set Max Pack Size,size must under 4194303 and upper 131,072,default size 262144.
	* @author  	     : byTangs
	* @param NewMaxSize : send data max size
	* @return  		 : void
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
    void SetMaxPackSize(const int64& NewMaxSize);



	/**
	* @brief   		    : set keep alive time(ms),default 60 *1000 . Do not send heartbeat packets if KeepAliveTime to be set 0
	* @author  	        : byTangs
	* @param KeepAliveTime : KeepAliveTime
	* @return  		    : void
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
    void SetKeepAliveTime(const int64& KeepAliveTime);


	/**
	* @brief   		    	   : set keep alive time Interval(ms),default 20 * 1000
	* 						     If the heartbeat is not detected, the packet is considered disconnected
	* 							 [Default: WinXP 5 times, Win7 10 times]
	* 							 Do not send heartbeat packets if KeepAliveTime to be set 0
	* @author  	               : byTangs
	* @param KeepAliveInterval : KeepAliveInterval
	* @return  		           : void
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpClient")
    void SetKeepAliveTimeInterval(const int64& KeepAliveInterval);

	
	/**
	 * @brief              : check connectionID is connected
	 * @author			   : byTangs
	 * @return		       : true if connected
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
	bool IsConnected()const;


	/**
	 * @brief   : check tcp client service has started
	 * @author  : byTangs
	 * @return  : true if already started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "HasStarted"), Category = "HPTcpSocket | TcpClient")
	bool HasStarted()const;


	/**
	* @brief   : Get Max Pack Size
	* @author  : byTangs
	* @returns : -1 if tcp Client invalid or not started
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
    int64 GetMaxPackSize() const;


	/**
	 * @brief   : Get Tcp Client Keep Alive Time,in milliseconds
	 * @author  : byTangs
	 * @return  : -1 if tcp Client not started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
    int64 GetKeepAliveTime()const;

	/**
	 * @brief   : Get Tcp Client Keep Alive Time Interval, in milliseconds
	 * @author  : byTangs
	 * @return  : -1 if tcp Client not started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
    int64 GetKeepAliveTimeInterval()const;

	
	/**
	 * @brief   : GetConnectionID
	 * @author  : byTangs
	 * @return  : -1 if tcp Client not connected
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
	int64 GetConnectionID()const;


	/**
	 * @brief   : Get Tcp Client State
	 * @author  : byTangs
	 * @return  : not ready ,starting ,started,stopping,stopped
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpClient")
    ETcpClientState GetTcpClientState()const;


	
	/**
	 * @brief     		   : GetLocalAddress
	 * @author  		   : byTangs 
	 * @param IPAddress    :  
	 * @param AppendPort   :  
	 * @return  		   : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetLocalAddress(FString& IPAddress,bool AppendPort = false)const;


	/**
	 * @brief     		   : GetRemoteHost
	 * @author  		   : byTangs 
	 * @param RemoteHost   :  
	 * @param AppendPort   :  
	 * @return  		   : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetRemoteAddress(FString& RemoteHost,bool AppendPort = false)const;


	/**
	* @brief   		       : Gets the length of unemitted data in a connection
	* @author 		       : byTangs
	* @param Pending 	   :  
	* @return  	           : true if success
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetPendingDataLength(int32& Pending)const;

	
	/**
	* @brief   : get tcp server listener
	* @author  : byTangs
	* @return  : 
	*/
	TSharedPtr<FTcpClientListener, ESPMode::ThreadSafe> GetTcpClientListener()const
	{
		return TcpClientListener;
	}


	
private:
	
	 /**
	  * @brief			: BindClientDelegate
	  * @author		    : byTangs
	  * @param Options  : TcpClientStartOptions
	  * @returns        : void
	  */
	  void BindClientDelegate(const FTcpClientStartOptions& Options);


	  /**
	   * @brief   : ApplyTcpClientSettings
	   * @author  : byTangs
	   * @returns : void
	   */
	   void ApplyTcpClientSettings();
	
private:

	/**  Tcp Pack client Listener Smart Pointer.*/
	TSharedPtr<FTcpClientListener, ESPMode::ThreadSafe> TcpClientListener;

	/**  All Delegate */
	FTcpClientStartOptions TcpClientStartOptions;
};
