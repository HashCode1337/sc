// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HAL/Runnable.h"

#include "windows/AllowWindowsPlatformTypes.h"
#include "SocketInterface.h"
#include "HPSocket.h"
#include "HPTypeDef.h"
#include "windows/HideWindowsPlatformTypes.h"




/**
 *  Delegate type for accepted TCP connections.
 *  The  parameter is the unique  id of connection
 */
DECLARE_DELEGATE_OneParam(FOnConnectedDelegate,const int64);


/**
 *  Delegate type for receive .
 *  parameter is the received message
 */
DECLARE_DELEGATE_OneParam(FOnClientReceiveMessageDelegate,const FString);


/**
 *  Delegate type for Close .
 *  The first parameter is the unique  id of connection
 *  The second parameter is the error code
 */
DECLARE_DELEGATE_TwoParams(FOnClientCloseDelegate, const int64, const int32);


/**
*  Delegate type for ReceiveBytes .
*  The first parameter is the unique  id of connection
*  The second parameter is the Received Bytes
*/
DECLARE_DELEGATE_TwoParams(FOnClientReceiveBytesDelegate, const int64, const TArray<uint8>&);



class FTcpClientListener : public FRunnable,public ITcpClientListener
{
public:
	
	FTcpClientListener();

	~FTcpClientListener();


public:

	//---------------------Runnable Thread Interface ----------------------//
	virtual bool Init()  override;

	/** thread run body */
	virtual uint32 Run() override;

	/** thread stop */
	virtual void Stop()  override { bStopping = true; }

	
	/** Destructor. */
	virtual void Exit()  override { }

public:

	//---------------------ITcpServerListener  Interface ----------------------//
	
	virtual EnHandleResult OnPrepareConnect(ITcpClient* Sender, CONNID ConnID, SOCKET socket)					  override { return HR_IGNORE; }

	virtual EnHandleResult OnConnect(ITcpClient* Sender, CONNID ConnID)											  override;

	virtual EnHandleResult OnHandShake(ITcpClient* Sender, CONNID ConnID)								          override { return HR_IGNORE; }

	virtual EnHandleResult OnReceive(ITcpClient* Sender, CONNID ConnID, const BYTE* Data, int Length)			  override;

	virtual EnHandleResult OnReceive(ITcpClient* Sender, CONNID ConnID, int Length)								  override { return HR_IGNORE; }
	
	virtual EnHandleResult OnSend(ITcpClient* Sender, CONNID ConnID, const BYTE* Data, int Length)				  override { return HR_IGNORE; }

	virtual EnHandleResult OnClose(ITcpClient* Sender, CONNID ConnID, EnSocketOperation Operation,int ErrorCode)  override;

public:

	/**
	 * Gets a delegate to be invoked when an incoming connection has been accepted.
	 */
	FOnConnectedDelegate& OnConnectedDelegate()
	{
		return ConnectedDelegate;
	}

	
	/**
	 * Gets a delegate to be invoked when receive message 
	 */
	FOnClientReceiveMessageDelegate&			   OnReceiveDelegate()
	{
		return ReceiveDelegate;
	}

	/**
	* Gets a delegate to be invoked when receive bytes 
	*/
	FOnClientReceiveBytesDelegate&			   OnClientReceiveBytesDelegate()
	{
		return ReceiveBytesDelegate;
	}

	/**
	 * Gets a delegate to be invoked when an incoming disconnected
	 *
	 */
	FOnClientCloseDelegate&              OnCloseDelegate()
	{
		return CloseDelegate;
	}


	
	/**
	 *  Get Tcp Pack Server Pointer
	 */
	ITcpPackClient* GetTcpClient() const 
	{
		return TcpPackClient;
	}
	
private:

	/** TcpPackServer */
	ITcpPackClient*		TcpPackClient = nullptr;

	/** Runnable Thread */
	FRunnableThread*    Thread		  = nullptr;


	/** Loop */
	bool bStopping;

	/** Holds a delegate to be invoked when an incoming connection has been accepted. */
	FOnConnectedDelegate 			ConnectedDelegate;

	/** Receive Delegate */
	FOnClientReceiveMessageDelegate ReceiveDelegate;

	/** Receive bytes Delegate */
	FOnClientReceiveBytesDelegate 	ReceiveBytesDelegate;

	
	/** Close Delegate */
	FOnClientCloseDelegate 			CloseDelegate;

	/** receive bytes*/
	TArray<uint8> 					Bytes;

	/** Bytes Length*/
	int64 							BytesLength = 0;

	/** Current Byte Read*/
	int64 							ByteRead = 0;

	/** Data Type */
	FString 						DataType;

	/** Receive File Name */
	FString 						Filename;

};
