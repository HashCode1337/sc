// Copyright 2020 byTangs, All Rights Reserved.


#include "TcpClientListener.h"
#include "HpSocketUtilities.h"
#include "HPTcpSocketCoreMinimal.h"
#include "HAL/ThreadingBase.h"
#include "Kismet/KismetStringLibrary.h"
#include "windows/WindowsPlatformProcess.h"
#include "FileToosBlueprintFunctionLibrary.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "HAL/PlatformFilemanager.h"
#include "HPTcpClientSettings.h"

FTcpClientListener::FTcpClientListener()
{
	Thread = FRunnableThread::Create(this, TEXT("TcpPackClientListener"), 8 * 1024, TPri_AboveNormal);
}

FTcpClientListener::~FTcpClientListener()
{
    if (Thread != nullptr)
    {
	    Thread->Kill(true);
	    delete Thread;
	    Thread = nullptr;
	    HP_LOG(Warning, TEXT("Stop Tcp Pack Client Listener..."));
    }

    if (TcpPackClient != nullptr)
    {
	    if(TcpPackClient->HasStarted())
	    {
		    TcpPackClient->Stop();
	    }
	    HP_Destroy_TcpPackClient(TcpPackClient);
	    TcpPackClient = nullptr;
    }

    HP_LOG(Warning, TEXT("Destory Tcp Client Listener... "));
}

bool FTcpClientListener::Init()
{
    TcpPackClient = HP_Create_TcpPackClient(this);
    return TcpPackClient != nullptr;
}

uint32 FTcpClientListener::Run()
{
    while (!bStopping)
    {
        FPlatformProcess::Sleep(5.f);
    }

    return 0;
}

EnHandleResult FTcpClientListener::OnConnect(ITcpClient* Sender, CONNID ConnID)
{
    ConnectedDelegate.ExecuteIfBound(ConnID);
    return HR_OK;
}

EnHandleResult FTcpClientListener::OnReceive(ITcpClient* Sender, CONNID ConnID, const BYTE* Data, int Length)
{
    std::string ReceiveDataHead(reinterpret_cast<const char*>(Data), Length);
    FString UnrealReceiveDataHead = UTF8_TO_TCHAR(ReceiveDataHead.c_str());
    TArray<FString> HeadParse;
    UnrealReceiveDataHead.ParseIntoArray(HeadParse,TEXT(":"));

    if(HeadParse.Num() == 3 && HeadParse[0].IsNumeric())
    {
        HP_LOG(Display,TEXT("Parsed Data Head Info"));
        
        BytesLength = FCString::Atoi64(*HeadParse[0]);
        HP_LOG(Warning,TEXT("Receive File Szie %lld"),BytesLength);
        
        DataType    = HeadParse[1];
        const UHPTcpClientSettings* TcpClientSettings = GetDefault<UHPTcpClientSettings>();

        const FString DefaultSavePath = TcpClientSettings ? TcpClientSettings->GetFileSavePath() : FPaths::ProjectSavedDir();

        Filename    = DefaultSavePath / HeadParse[2];
        Bytes.Empty();
    }else
    {
        if( DataType.Equals(TEXT("Message")))
        {
            if (BytesLength <= TcpPackClient->GetMaxPackSize())
            {
                std::string RealMessage(reinterpret_cast<const char*>(Data), Length);
                FString UnrealMessage = UTF8_TO_TCHAR(RealMessage.c_str());
                ReceiveDelegate.ExecuteIfBound(UnrealMessage);
            }
            else
            {
                Bytes.Append(Data, Length);
                ByteRead += Length;
                if (ByteRead == BytesLength)
                {
                    std::string RealMessage(reinterpret_cast<const char*>(Bytes.GetData()), BytesLength);
                    FString UnrealMessage = UTF8_TO_TCHAR(RealMessage.c_str());
                    ReceiveDelegate.ExecuteIfBound(UnrealMessage);
                    Bytes.Empty();
                    ByteRead = 0;
                    BytesLength = 0;
                }
            }
        }else
        {
            if (BytesLength <= TcpPackClient->GetMaxPackSize())
            {
                Bytes.Append(Data,Length);
                ReceiveBytesDelegate.ExecuteIfBound(ConnID,Bytes);
            }
            else
            {
                Bytes.Append(Data, Length);
                ByteRead += Length;
                if (ByteRead == BytesLength)
                {
                    ReceiveBytesDelegate.ExecuteIfBound(ConnID,Bytes);
                    Bytes.Empty();
                    ByteRead = 0;
                    BytesLength = 0;
                }
            }
  
        }               
    }
        
    return HR_OK;
}


EnHandleResult FTcpClientListener::OnClose(ITcpClient* Sender, CONNID ConnID, EnSocketOperation Operation,
                                           int ErrorCode)
{
    CloseDelegate.ExecuteIfBound(ConnID, Operation);
    return HR_OK;
}
