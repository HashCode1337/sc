// Copyright 2020 byTangs, All Rights Reserved.


#include "HPTcpClientSettings.h"

UHPTcpClientSettings::UHPTcpClientSettings()
	:AbnormalAliveTimeInterval(20000)
	,KeepAliveTime(60000)
	,MaxSocketBufferSize(4096)
	,MaxPackSize(262144)
{

}


FString UHPTcpClientSettings::GetFileSavePath()const
{
	return FileSavePath.Path;
}

int64 UHPTcpClientSettings::GetAbnormalAliveTimeInterval() const
{
	return AbnormalAliveTimeInterval;
}

int64 UHPTcpClientSettings::GetKeepAliveTimeInterval() const
{
	return KeepAliveTime;
}

int64 UHPTcpClientSettings::GetSocketBufferSize() const
{
	return MaxSocketBufferSize;
}
 
int64 UHPTcpClientSettings::GetMaxPackSize() const
{
	return MaxPackSize;
}


#if WITH_EDITOR
FText UHPTcpClientSettings::GetSectionText() const
{
	return FText::FromString(TEXT("HPTcpClientSettings")); 
}
#endif


FName UHPTcpClientSettings::GetCategoryName() const
{
	return FName(TEXT("Plugins"));
}