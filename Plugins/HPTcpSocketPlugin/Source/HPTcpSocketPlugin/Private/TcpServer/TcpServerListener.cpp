// Copyright 2020 byTangs, All Rights Reserved.


#include "TcpServerListener.h"

#include "FileToosBlueprintFunctionLibrary.h"
#include "HpSocketUtilities.h"
#include "HAL/ThreadingBase.h"
#include "windows/WindowsPlatformProcess.h"
#include "HPTcpSocketCoreMinimal.h"
#include "Kismet/KismetStringLibrary.h"
#include "HAL/PlatformFilemanager.h"
#include "Misc/Paths.h"
#include "HPTcpSocketSettings.h"


FTcpServerListener::FTcpServerListener()
{
    Thread = FRunnableThread::Create(this, TEXT("TcpPackServerListener"), 8 * 1024, TPri_AboveNormal);
}

FTcpServerListener::~FTcpServerListener()
{
    if (Thread != nullptr)
    {
	    Thread->Kill(true);
	    delete Thread;
	    Thread = nullptr;
	    HP_LOG(Warning, TEXT("Stop Tcp Pack Server Listener..."));
    }

    if (TcpPackServer != nullptr)
    {
        if(TcpPackServer->HasStarted())
        {
            TcpPackServer->Stop();
            HP_LOG(Warning, TEXT("Stop Tcp  Server ..."));
        }

        HP_Destroy_TcpPackServer(TcpPackServer);
        TcpPackServer = nullptr;
    }

 
    
    HP_LOG(Warning, TEXT("Destory Tcp Server  Listener... "));
}

bool FTcpServerListener::Init()
{
    TcpPackServer = HP_Create_TcpPackServer(this);
    return TcpPackServer != nullptr;
}

uint32 FTcpServerListener::Run()
{
    while (!bStopping)
    {
        FPlatformProcess::Sleep(5.f);
    }

    return 0;
}

EnHandleResult FTcpServerListener::OnAccept(ITcpServer* Sender, CONNID ConnID, UINT_PTR Client)
{
    ConnectionAcceptedDelegate.ExecuteIfBound(ConnID);
    TSharedPtr<FServerReceiveMatcher> Matcher = MakeShareable(new FServerReceiveMatcher(ConnID,this));
    if(Matcher != nullptr)
    {
        TTuple<CONNID,TSharedPtr<FServerReceiveMatcher>> Pair { ConnID,Matcher};
        ServerReceiveMatcher.Add(MoveTemp(Pair));
    }
    return HR_OK;
}

EnHandleResult FTcpServerListener::OnReceive(ITcpServer* Sender, CONNID ConnID, const BYTE* Data, int Length)
{
    TArray<uint8> ReceiveData;
    ReceiveData.SetNumUninitialized(Length);
    FMemory::Memcpy(ReceiveData.GetData(),Data,Length);
    if(ServerReceiveMatcher.Contains(ConnID))
    {
       ServerReceiveMatcher.FindRef(ConnID)->HandleReceiveData(ReceiveData,Length);
    }

    return HR_OK;
}

EnHandleResult FTcpServerListener::OnShutdown(ITcpServer* Sender)
{
    ShotDownDelegate.ExecuteIfBound();
    return HR_OK;
}


EnHandleResult FTcpServerListener::OnClose(ITcpServer* Sender, CONNID ConnID, EnSocketOperation Operation,
	int ErrorCode)
{
    CloseDelegate.ExecuteIfBound(ConnID, Operation);

    if(ServerReceiveMatcher.Contains(ConnID))
    {
        ServerReceiveMatcher.Remove(ConnID);
    }
    return HR_OK;
}





FServerReceiveMatcher::FServerReceiveMatcher(const CONNID InConnectionID, FTcpServerListener* InTcpServerListener)
    :ConnectionID(InConnectionID) ,TcpServerListener(InTcpServerListener)
{
    
}

EnHandleResult FServerReceiveMatcher::HandleReceiveData(const TArray<uint8>& Data, int Length)
{
    if( TcpServerListener == nullptr)
        return HR_IGNORE;
 
    std::string ReceiveDataHead(reinterpret_cast<const char*>(Data.GetData()), Length);
    FString UnrealReceiveDataHead = UTF8_TO_TCHAR(ReceiveDataHead.c_str());
    HP_LOG(Display,TEXT("Parsed Data Head Info %s"),*UnrealReceiveDataHead);
    TArray<FString> HeadParse;
    UnrealReceiveDataHead.ParseIntoArray(HeadParse,TEXT(":"));

    // Length , Type ,Filename
    if(HeadParse.Num() == 3 && HeadParse[0].IsNumeric())
    {
        HP_LOG(Display,TEXT("Parsed Data Head Info"));
        BytesLength = FCString::Atoi64(*HeadParse[0]);
        HP_LOG(Warning,TEXT("Receive File Szie %lld"),BytesLength);
        DataType    = HeadParse[1];

        const UHPTcpServerSettings* TcpClientSettings = GetDefault<UHPTcpServerSettings>();
        const FString DefaultSavePath = TcpClientSettings ? TcpClientSettings->GetFileSavePath() : FPaths::ProjectSavedDir();

        Filename    = DefaultSavePath / HeadParse[2];
        Bytes.Empty();
    }else
    {
        if( DataType.Equals(TEXT("Message")))
        {
            if (BytesLength <= TcpServerListener->GetTcpServer()->GetMaxPackSize())
            {
                std::string RealMessage(reinterpret_cast<const char*>(Data.GetData()), Length);
                FString UnrealMessage = UTF8_TO_TCHAR(RealMessage.c_str());
                TcpServerListener->OnServerReceiveMessageDelegate().ExecuteIfBound(ConnectionID, UnrealMessage);
            }
            else
            {
                Bytes.Append(Data.GetData(), Length);
                ByteRead += Length;
                if (ByteRead == BytesLength)
                {
                    std::string RealMessage(reinterpret_cast<const char*>(Bytes.GetData()), BytesLength);
                    FString UnrealMessage = UTF8_TO_TCHAR(RealMessage.c_str());
                    TcpServerListener->OnServerReceiveMessageDelegate().ExecuteIfBound(ConnectionID, UnrealMessage);
                    Bytes.Empty();
                    ByteRead = 0;
                    BytesLength = 0;
                }
            }
        }else if (DataType.Equals(TEXT("Bytes")))
        {
            if (BytesLength <= TcpServerListener->GetTcpServer()->GetMaxPackSize())
            {
                Bytes.Append(Data.GetData(),Length);
                TcpServerListener->OnServerReceiveBytesDelegate().ExecuteIfBound(ConnectionID,Bytes);
            }
            else
            {
                Bytes.Append(Data.GetData(), Length);
                ByteRead += Length;
                if (ByteRead == BytesLength)
                {
                    TcpServerListener->OnServerReceiveBytesDelegate().ExecuteIfBound(ConnectionID,Bytes);
                    Bytes.Empty();
                    ByteRead = 0;
                    BytesLength = 0;
                }
            }
        }
    }      
    return HR_OK;
}


FServerReceiveMatcher::~FServerReceiveMatcher()
{
    HP_LOG(Warning,TEXT("Destory Receive Matcher..."));
}