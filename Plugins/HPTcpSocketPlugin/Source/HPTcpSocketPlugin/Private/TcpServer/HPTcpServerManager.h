// Copyright 2020 byTangs, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TcpServer/TcpServerListener.h"
#include "HPTcpServerManager.generated.h"



DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAccept, const int64,ConnectionID);

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnServerReceiveMessage, const int64, ConnectionID, const FString,Message);

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnServerReceiveBytes, const int64, ConnectionID, const TArray<uint8>&,Bytes);

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnServerClose, const int64, ConnectionID,const int32,ErrorCode);

DECLARE_DYNAMIC_DELEGATE(FOnShotDown);


UENUM(BlueprintType,Blueprintable)
enum class EBIPAddressType : uint8
{
    ALL = 0,
    IPV4,
    IPV6
};


UENUM(BlueprintType,Blueprintable)
enum class ETcpServerState : uint8
{
	STARTING = 0  UMETA(DisplyName = "Starting"),
	STARTED		  UMETA(DisplyName = "Started"),
	STOPPING      UMETA(DisplyName = "Stopping"),
	STOPPED  	  UMETA(DisplyName = "Stopped"),
	NOTREADY      UMETA(DisplyName = "NotReady")
};



/**
 * Options specific to start tcp socket
 */
USTRUCT(BlueprintType)
struct FTcpServerStartOptions
{
	GENERATED_BODY()

	/** A callback to invoke when has new connection*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpServer")
	FOnAccept OnAccept;

	/** A callback to invoke when receive message */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpServer")
	FOnServerReceiveMessage OnReceiveMessage;

	/** A callback to invoke when receive Bytes */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpServer")
	FOnServerReceiveBytes  OnReceiveBytes;
	
	/** A callback to invoke when connection disconnection */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpServer")
	FOnServerClose OnClose;

	/** A callback to invoke when server shutdown */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HPTcpSocket | TcpServer")
	FOnShotDown OnShotDown;

};


/**
 * 
 */
UCLASS(BlueprintType)
class HPTCPSOCKETPLUGIN_API UHPTcpServerManager final : public UObject
{
	GENERATED_BODY()

public:
	
	/**
	 * @brief   : Tcp Server Manager Destructor
	 * @author  : byTangs
	 * @returns :
	 */
	~UHPTcpServerManager();


	
	/**
	 * @brief         : Start Tcp Pack Server
	 * @author	      : byTangs
	 * @param Ip      : Listen ip
	 * @param Port    : Listen port
	 * @param Options : Tcp Server Start Options
	 * @return        : true if success
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	bool StartTcpServer(const FString& Ip, const int32 Port, const FTcpServerStartOptions& Options);


	
	/**
	 * @brief   : close tcp server 
	 * @author  : byTangs
	 * @return  : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void CloseTcpServer();


	/**
	 * @brief				: disconnect connection
	 * @author				: byTangs
	 * @param ConnectionId  : the connection id to be disconnect
	 * @return				: true if success
	 */
	UFUNCTION(BlueprintCallable,Category = "HPTcpSocket | TcpServer")
	bool Disconnect(const int64& ConnectionId);

	
	/**
	 * @brief				: send data to special connection
	 * @author				: byTangs
	 * @param ConnectionID  : the connection to be sent 
	 * @param Message		: data
	 * @return				: bool
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	bool SendMessage(const int64& ConnectionID,const FString& Message);


	
	/**
	 * @brief   		   : Send Bytes To Connection
	 * @author  		   : byTangs
	 * @param ConnectionID :  
	 * @param Bytes 	   :  
	 * @return             : true if success
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	bool SendBytes(const int64& ConnectionID, const TArray<uint8>& Bytes);
	
	/**
	 * @brief   		: Set the maximum number of connections (the component will pre-allocate memory according to the set value,
	 * so it needs to be set according to the actual situation, not too large)
	 * @author  		: byTangs
	 * @param NewCount  : Max count of connection
	 * @return          : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetMaxConnectionCount(const int64& NewCount);


	
	/**
	 * @brief   		  : Set whether to mark the silent time
	 * 						DisconnectSilenceConnections() , GetSilencePeriod() is valid when MarkSilence is to be set true
	 * @author  		  : byTangs
	 * @param MarkSilence : MarkSilence
	 * @return  		  : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetMarkSilence(bool MarkSilence = true);


	
	/**
	 * @brief   		    : set keep alive time(ms),default 60 *1000 . Do not send heartbeat packets if KeepAliveTime to be set 0
	 * @author  	        : byTangs
	 * @param KeepAliveTime : KeepAliveTime
	 * @return  		    : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetKeepAliveTime(const int64& KeepAliveTime);


	/**
	* @brief   		    	   : set keep alive time Interval(ms),default 20 * 1000
	* 						     If the heartbeat is not detected, the packet is considered disconnected
	* 							 [Default: WinXP 5 times, Win7 10 times]
	* 							 Do not send heartbeat packets if KeepAliveTime to be set 0
	* @author  	               : byTangs
	* @param KeepAliveInterval : KeepAliveInterval
	* @return  		           : void
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
    void SetKeepAliveTimeInterval(const int64& KeepAliveInterval);

	
	/**
	 * @brief   		 : Set Max Pack Size,size must under 4194303 and upper 131,072,default size 262144.
	 * @author  	     : byTangs
	 * @param NewMaxSize : send data max size
	 * @return  		 : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetMaxPackSize(const int64& NewMaxSize);

	/**
	 * @brief             : Set Accept Socket Count,Depending on the load adjustment Settings, the larger the number of Accept preposts, the more concurrent connection requests are supported
	 * @author            : byTangs
	 * @param AcceptCount :  
	 * @return  	      : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetAcceptSocketCount(const int64& AcceptCount);


	
	/**
	 * @brief   			   : SetSocketBufferSize,Set the communication data buffer size (adjusted for the average communication packet size, usually a multiple of 1024)
	 * @author  		       : byTangs
	 * @param SocketBufferSize :  
	 * @return  			   : void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetSocketBufferSize(const int64& SocketBufferSize);

	
	/**
	 * @brief   				: SetSocketListenQueue,Set the waiting queue size for listening sockets (adjust the setting according to the number of concurrent connections)
	 * @author  				: byTangs
	 * @param SocketListenQueue :  
	 * @return 					: void
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	void SetSocketListenQueue(const int64& SocketListenQueue);


	
	/**
	 * @brief        : Disconnect the connection for longer than specified
	 * @author       : byTangs
	 * @param Period :  
	 * @param bForce :  
	 * @return       : true if disconnected success
	 */
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
	bool DisconnectLongConnections(int64 Period,bool bForce = true);


	
	/**
	* @brief        : Disconnect the silent connection for longer than specified
	* @author       : byTangs
	* @param Period :  
	* @param bForce :  
	* @return       : true if disconnected success
	*/
	UFUNCTION(BlueprintCallable, Category = "HPTcpSocket | TcpServer")
    bool DisconnectSilenceConnections(int64 Period,bool bForce = true);
	
	
	/**
	 * @brief             : get the name of machine
	 * @author            : byTangs
	 * @param OutHostName : the name of machine
	 * @return            : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (CompactNodeTitle = "HostName "), Category = "HPTcpSocket")
	static bool GetHostName(FString& OutHostName);


	
	/**
	 * @brief			    : EnumIPAddress
	 * @author              : byTangs
	 * @param IPAddressType : ip address type,ipv4,ipv6 or all 
	 * @param OutAddress    : out ip address array
	 * @return              : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure,Category = "HPTcpSocket")
	static bool EnumIPAddress(const EBIPAddressType IPAddressType, TArray<FString>& OutAddress);

	
	/**
	 * @brief           : check ip address is valid
	 * @author          : byTangs
	 * @param InAddress : IP address to be checked
	 * @return			: true if valid
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket")
	static bool IPIsValid(const FString& InAddress);


	/**
	 * @brief   : check tcp server has started
	 * @author  : byTangs
	 * @return  : true if already started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "HasStarted"), Category = "HPTcpSocket | TcpServer")
	bool HasStarted()const;


	/**
	 * @brief              : check connectionID is connected
	 * @author			   : byTangs
	 * @param ConnectionID : the id to be checked
	 * @return		       : true if connected
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure,Category = "HPTcpSocket | TcpServer")
	bool IsConnected(const int64& ConnectionID)const;

	
	/**
	 * @brief   : get connection count
	 * @author  : byTangs
	 * @return  : number of connection
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	int64 GetConnectionCount()const;


	
	/**
	 * @brief			: get all connection unique id
	 * @author			: byTangs
	 * @param OutConnID : all connection ID
	 * @return			: true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool GetAllConnectionIDs(TArray<int64>& OutConnID)const;


	/**
	 * @brief   : Get Max Pack Size
	 * @author  : byTangs
	 * @returns : -1 if tcp server invalid or not started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	int64 GetMaxPackSize() const;


	
	/**
	 * @brief  			  : Get Tcp Server Listen Address,return address with port( such as 0.0.0.0:8888) if bAppendPort to be set true.
	 * return empty if server not started.
	 * @author  		  : byTangs
	 * @param bAppendPort : address with port if true
	 * @return  		  : Server Listen Address
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	FString GetListenAddress(bool bAppendPort)const;


	
	/**
	 * @brief   		   : Get Connection Local Address
	 * @author  		   : byTangs
	 * @param ConnectionID : special connection id 
	 * @param OutAddress   : out local address
	 * @param bAppendPort  : address with port if true
	 * @return  	       : true if get address success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool GetConnectionLocalAddress(const int64& ConnectionID, FString& OutAddress, bool bAppendPort)const;


	/**
	 * @brief   		   : Get Connection remote Address
	 * @author  		   : byTangs
	 * @param ConnectionID : special connection id 
	 * @param OutAddress   : out remote address
	 * @param bAppendPort  : address with port if true
	 * @return  	       : true if get address success
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetConnectionRemoteAddress(const int64& ConnectionID, FString& OutAddress, bool bAppendPort)const;

	
	/**
	 * @brief   : Get Tcp Server State
	 * @author  : byTangs
	 * @return  : not ready ,starting ,started,stopping,stopped
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	ETcpServerState GetTcpServerState()const;

	
	/**
 	 * @brief   			: Get Special Connection Period(ms)
	 * @author   			: byTangs
	 * @param ConnectionID  : the connection id
	 * @param Period		: the connection period
	 * @return  		    : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetConnectionPeriod(const int64& ConnectionID, FString& Period)const;
	
	
	/**
	 * @brief   : 
	 * @author  : byTangs
	 * @param ConnectionID :  
	 * @param Period :  
	 * @return  : bool
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetConnectionSilencePeriod(const int64& ConnectionID, FString& Period)const;

	/**
	 * @brief   : Get Max Connection Count
	 * @author  : byTangs
	 * @return  : -1 if tcp server not started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	int64 GetMaxConnectionCount()const;


	
	/**
	 * @brief   : check tcp server is marked Silence time
	 * @author  : byTangs
	 * @return  : true if marked ,false if server not started
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool IsMarkSilence()const;


	/**
	 * @brief   : Get Tcp Server Keep Alive Time
	 * @author  : byTangs
	 * @return  : -1 if tcp server not invalid
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	int64 GetKeepAliveTime()const;


	
	/**
	* @brief   : Get Tcp Server Keep Alive Time Interval
	* @author  : byTangs
	* @return  : -1 if tcp server not invalid
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    int64 GetKeepAliveTimeInterval()const;


	
	/**
	 * @brief   : GetAcceptSocketCount
	 * @author  : byTangs
	 * @return  : -1 if tcp server not invalid
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	int64 GetAcceptSocketCount()const;


	/**
	 * @brief   : GetSocketBufferSize
	 * @author  : byTangs
	 * @return  : -1 if tcp server invalid
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    int64 GetSocketBufferSize()const;

	
	/**
 	 * @brief   : GetSocketListenQueue
	 * @author  : byTangs
	 * @return  : -1 if tcp server invalid
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    int64 GetSocketListenQueue()const;


	
	/**
	 * @brief     		   : How long it takes to get a connection (in seconds)
	 * @author  		   : byTangs
	 * @param ConnectionID :
	 * @param Period	   :
	 * @return  	       : bool
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool GetConnectPeriod(const int64& ConnectionID, int64& Period)const;


	/**
	 * @brief     		   : How long it takes to get a connection (in seconds)
	 * @author  		   : byTangs
	 * @param ConnectionID :
	 * @param SilencePeriod:
	 * @return  	       : true if success
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetSilencePeriod(const int64& ConnectionID, int64& SilencePeriod)const;

	
	/**
	 * @brief     		   : GetLocalAddress
	 * @author  		   : byTangs
	 * @param ConnectionID :  
	 * @param IPAddress    :  
	 * @param AppendPort   :  
	 * @return  		   : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool GetLocalAddress(const int64& ConnectionID,FString& IPAddress,bool AppendPort = false)const;


	/**
	 * @brief     		   : GetRemoteAddress
	 * @author  		   : byTangs
	 * @param ConnectionID :  
	 * @param IPAddress    :  
	 * @param AppendPort   :  
	 * @return  		   : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
    bool GetRemoteAddress(const int64& ConnectionID,FString& IPAddress,bool AppendPort = false)const;


	
	/**
	 * @brief   		   : Gets the length of unemitted data in a connection
	 * @author 		       : byTangs
	 * @param ConnectionID :  
	 * @param Pending 	   :  
	 * @return  	       : true if success
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	bool GetPendingDataLength(const int64& ConnectionID,int32& Pending)const;


	
	/**
	 * @brief   : check server is in sending state
	 * @author  : byTangs
	 * @return  : bool
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HPTcpSocket | TcpServer")
	FORCEINLINE bool IsSending()const
	{
		return bIsSending;
	}


	
	/**
	 * @brief       : convert bytes to string
	 * @author      : byTangs
	 * @param Bytes :  
	 * @return      : FString
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "ToString (Bytes)", CompactNodeTitle = "->",BlueprintAutocast),Category = "HPTcpSocket")
	static FString BytesToString(const TArray<uint8>& Bytes); 


	
	/**
	 * @brief        : convert string to bytes
	 * @author       : byTangs
	 * @param String :  
	 * @param Bytes  :  
	 * @return       : void
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "ToBytes (String)", CompactNodeTitle = "->",BlueprintAutocast),Category = "HPTcpSocket")
	static void StringToBytes(const FString& String,TArray<uint8>& Bytes);


	
	/**
	 * @brief   : get tcp server listener
	 * @author  : byTangs
	 * @return  : 
	 */
	TSharedPtr<class FTcpServerListener, ESPMode::ThreadSafe> GetTcpServerListener() const
	{
		return TcpServerListener;
	}

	
	/**
	 * @brief  		    : set server send state
	 * @author          : byTangs
	 * @param SendState : sending or false
	 * @return  		: void
	 */
	void SetIsSending(bool SendState);


	
private:
	
    /**
	 * @brief   : Bind Server Delegate
	 * @author  : byTangs
	 * @return  : void
	 */
	void BindServerDelegate(const FTcpServerStartOptions &Options);


	/**
	 * @brief   : Apply TcpServer Settings
	 * @author  : byTangs
	 * @returns : void
	 */
	 void ApplyTcpServerSettings();

private:

	/**  TcpPackServer Listener Smart Pointer.*/
	TSharedPtr<class FTcpServerListener, ESPMode::ThreadSafe> TcpServerListener;

	/** StartOptions */
	FTcpServerStartOptions TcpServerStartOptions;
	
	/** server is sending to connection */
	bool bIsSending = false;

};
