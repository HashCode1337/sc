// Copyright 2020 byTangs, All Rights Reserved.


#include "HPTcpSocketSettings.h"

UHPTcpServerSettings::UHPTcpServerSettings()
	:AbnormalAliveTimeInterval(20000)
	,KeepAliveTime(60000)
	,MaxAcceptSocketCount(300)
	,MaxSocketBufferSize(4096)
	,MaxSocketListenQueue(1000)
	,bIsMarkSilence(true)
	,MaxPackSize(262144)
	,MaxConnectionCount(100)
{

}


FString UHPTcpServerSettings::GetFileSavePath()const
{
	return FileSavePath.Path;
}

int64 UHPTcpServerSettings::GetAbnormalAliveTimeInterval() const
{
	return AbnormalAliveTimeInterval;
}

int64 UHPTcpServerSettings::GetKeepAliveTimeInterval() const
{
	return KeepAliveTime;
}

int64 UHPTcpServerSettings::GetAcceptSocketCount() const
{
	return MaxAcceptSocketCount;
}

int64 UHPTcpServerSettings::GetSocketBufferSize() const
{
	return MaxSocketBufferSize;
}

int64 UHPTcpServerSettings::GetSocketListenQueue() const
{
	return MaxSocketListenQueue;
}

int64 UHPTcpServerSettings::GetMaxPackSize() const
{
	return MaxPackSize;
}

bool UHPTcpServerSettings::GetIsMarkSilence()const
{
	return bIsMarkSilence;
}


int64 UHPTcpServerSettings::GetMaxConnectionCount() const
{
	return MaxConnectionCount;
}

#if WITH_EDITOR
FText UHPTcpServerSettings::GetSectionText() const
{
	return FText::FromString(TEXT("HPTcpServerSettings"));
}
#endif


FName UHPTcpServerSettings::GetCategoryName() const
{
	return FName(TEXT("Plugins"));
}

