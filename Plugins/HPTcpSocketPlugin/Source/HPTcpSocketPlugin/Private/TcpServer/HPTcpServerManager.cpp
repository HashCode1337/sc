// Copyright 2020 byTangs, All Rights Reserved.


#include "HPTcpServerManager.h"
#include "HpSocketUtilities.h"
#include "Async/TaskGraphInterfaces.h"
#include "Async/Async.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "HAL/UnrealMemory.h"
#include <string>
#include "HPTcpSocketSettings.h"


UHPTcpServerManager::~UHPTcpServerManager()
{
    HP_LOG(Warning, TEXT("Destory Tcp Server Manager ... "));
}



bool UHPTcpServerManager::StartTcpServer(const FString& Ip, const int32 Port, const FTcpServerStartOptions& Options)
{
    if (!IPIsValidInternal(Ip))
    {
        return false;
    }
    
    if (!TcpServerListener.IsValid())
    {
        TcpServerListener = MakeShareable(new FTcpServerListener());
   
       if(!TcpServerListener.IsValid())
           return false;
    }

    BindServerDelegate(Options);
    ApplyTcpServerSettings();
    return  TcpServerListener.Get()->GetTcpServer()->Start(*Ip,Port);
}

void UHPTcpServerManager::CloseTcpServer()
{
    // make sure execute in main thread ---------------------------------------
    if(TcpServerListener.IsValid())
    {
        AsyncTask(ENamedThreads::GameThread,[this]()
        {
            if(this->HasStarted())
            {
                this->TcpServerListener.Get()->GetTcpServer()->Stop();
            }
        });      
    }
}

bool UHPTcpServerManager::Disconnect(const int64& ConnectionId)
{
    if (!TcpServerListener.IsValid() || !IsConnected(ConnectionId) ) return false;
    return TcpServerListener.Get()->GetTcpServer()->Disconnect(ConnectionId);
}

bool UHPTcpServerManager::SendMessage(const int64& ConnectionID, const FString& Message)
{
    if (!TcpServerListener.IsValid() || Message.IsEmpty() || bIsSending) return false;

    bIsSending = true;
    
    // Data Size
    const FString DataLength   = FString::FromInt(Message.Len() + 1);

    // send data type (default type : message,RawBytes,File)
    const FString SendDataType = TEXT("Message");

    // the name of file, only valid in file type
    const FString SendDataName = TEXT("NULL");

    // first send message info ,send send real data
    const FString SendDataHead = DataLength + TEXT(":") + SendDataType + TEXT(":") + SendDataName;
    
    const std::string DataHeadInfo = TCHAR_TO_UTF8(*SendDataHead);

    const std::string RealData     = TCHAR_TO_UTF8(*Message);

    const int32 DataRealSize = Message.Len() + 1;
    
    if(DataRealSize > GetMaxPackSize())
    {
        // send data head info first
        if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
            HP_LOG(Error,TEXT("Send Message To Client Failed , Error Code = %ul"),SYS_GetLastError());
            bIsSending = false;
            return false;
        }
        
        int64 RemainingSizeToSent = DataRealSize;

        //send data
        while(RemainingSizeToSent > 0 )
        {
            const int64 SizeToSent = FMath::Min(GetMaxPackSize(),RemainingSizeToSent);
            if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(RealData.c_str() + SizeToSent),SizeToSent))
            {
                HP_LOG(Error,TEXT("Send Message To Client Failed , Error Code = %ul"),SYS_GetLastError());
                bIsSending = false;
                return false;
            }
            RemainingSizeToSent -= SizeToSent;
        }
        bIsSending = false;
        return true;
    }else
    {
        if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
            HP_LOG(Error,TEXT("Send Message To Client Failed , Error Code = %ul"),SYS_GetLastError());
            bIsSending = false;
            return false;
        }

        const bool SentResult = TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(RealData.c_str()), RealData.length() + 1);
        bIsSending = false;
        return SentResult;
    }
}

bool UHPTcpServerManager::SendBytes(const int64& ConnectionID, const TArray<uint8>& Bytes)
{
    if(Bytes.Num() <= 0 || bIsSending) return false;

    bIsSending = true;

    
    const int32 DataRealSize = Bytes.Num();
    
    // Data Size
    const FString DataLength   = FString::FromInt(DataRealSize);

    // send data type (default type : message,RawBytes,File)
    const FString SendDataType = TEXT("Bytes");

    // the name of file, only valid in file type
   const FString SendDataName = TEXT("NULL");

    // first send messga info ,send send real data
    const FString SendDataHead = DataLength + TEXT(":") + SendDataType + TEXT(":") + SendDataName;
    
    const std::string DataHeadInfo = TCHAR_TO_UTF8(*SendDataHead);

    
    if(DataRealSize > GetMaxPackSize())
    {
   
        if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
            HP_LOG(Error,TEXT("Send Bytes To Client Failed , Error Code = %ul"),SYS_GetLastError());
            bIsSending = false;
            return false;
        }
        
        int64 RemainingSizeToSent = DataRealSize;
        while(RemainingSizeToSent > 0 )
        {
            const int64 SizeToSent = FMath::Min(GetMaxPackSize(),RemainingSizeToSent);
            if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(Bytes.GetData() + SizeToSent),SizeToSent))
            {
                HP_LOG(Error,TEXT("Send Bytes To Client Failed , Error Code = %ul"),SYS_GetLastError());
                bIsSending = false;
                return false;
            }
            RemainingSizeToSent -= SizeToSent;
        }
        bIsSending = false;
        return true;
    }else
    {
        if(!TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,reinterpret_cast<const BYTE*>(DataHeadInfo.c_str()), DataHeadInfo.length() + 1))
        {
            HP_LOG(Error,TEXT("Send Bytes To Client Failed , Error Code = %ul"),SYS_GetLastError());
            bIsSending = false;
            return false;
        }
        
        const bool SentResult = TcpServerListener.Get()->GetTcpServer()->Send(ConnectionID,Bytes.GetData(), DataRealSize);
        bIsSending = false;
        return SentResult;
    }
}


void UHPTcpServerManager::SetMaxConnectionCount(const int64& NewCount)
{  
    if(TcpServerListener.IsValid())
    {
        const DWORD  Count = UKismetMathLibrary::Clamp(NewCount,1,NewCount);
        TcpServerListener.Get()->GetTcpServer()->SetMaxConnectionCount(Count);
    }
}

void UHPTcpServerManager::SetMarkSilence(bool MarkSilence)
{
    if(TcpServerListener.IsValid())
    {
        TcpServerListener.Get()->GetTcpServer()->SetMarkSilence(MarkSilence);
    }
}

void UHPTcpServerManager::SetKeepAliveTime(const int64& KeepAliveTime)
{
    if(TcpServerListener.IsValid())
    {
        const DWORD Time = UKismetMathLibrary::Clamp(KeepAliveTime,0,KeepAliveTime);
        TcpServerListener.Get()->GetTcpServer()->SetKeepAliveTime(Time);
    }
}

void UHPTcpServerManager::SetKeepAliveTimeInterval(const int64& KeepAliveInterval)
{
    if(TcpServerListener.IsValid())
    {
        const DWORD Time = UKismetMathLibrary::Clamp(KeepAliveInterval,0,KeepAliveInterval);
        TcpServerListener.Get()->GetTcpServer()->SetKeepAliveTime(Time);
    }
}

void UHPTcpServerManager::SetMaxPackSize(const int64& NewMaxSize)
{
    if(TcpServerListener.IsValid() && NewMaxSize >= 131072)
    {
        const DWORD MaxSize = UKismetMathLibrary::Clamp(NewMaxSize,131072,4194303);
        TcpServerListener.Get()->GetTcpServer()->SetMaxPackSize(MaxSize);
    }
}


bool UHPTcpServerManager::GetHostName(FString& OutHostName)
{
    return  GetHostNameInternal(OutHostName);
}


bool UHPTcpServerManager::EnumIPAddress(const EBIPAddressType IPAddressType, TArray<FString>& OutAddress)
{
    return  GetLocalAddressInternal(static_cast<EIPAddressType>(IPAddressType), OutAddress);
}

bool UHPTcpServerManager::IPIsValid(const FString& InAddress)
{
    return IPIsValidInternal(InAddress);
}

bool UHPTcpServerManager::HasStarted()const
{
    if(!TcpServerListener.IsValid()) return false;

    return  TcpServerListener.Get()->GetTcpServer()->HasStarted();
}

bool UHPTcpServerManager::IsConnected(const int64& ConnectionID) const
{
    if (!TcpServerListener.IsValid()) return false;
    return TcpServerListener.Get()->GetTcpServer()->IsConnected(ConnectionID);
}

int64 UHPTcpServerManager::GetConnectionCount() const
{
    if (!TcpServerListener.IsValid()) return 0;
    return TcpServerListener.Get()->GetTcpServer()->GetConnectionCount();
}


bool UHPTcpServerManager::GetAllConnectionIDs(TArray<int64>& OutConnID)const
{
    if (!TcpServerListener.IsValid()) return false;
    DWORD Count = GetConnectionCount();
    OutConnID.Empty();
    CONNID* OutID = new CONNID[Count];

    if(OutID == nullptr)
        return false;
    
    if(!TcpServerListener.Get()->GetTcpServer()->GetAllConnectionIDs(OutID, Count))
    {
        delete[] OutID;
        return false;
    }
        
    for(DWORD i = 0 ; i < Count ; ++i)
    {
	    OutConnID.Emplace(Count + i);
    }

    delete[] OutID;
    return true;
}

void UHPTcpServerManager::SetAcceptSocketCount(const int64& AcceptCount)
{
    if (TcpServerListener.IsValid())
    {
        const DWORD Count = UKismetMathLibrary::Clamp(AcceptCount,0,AcceptCount);
        TcpServerListener.Get()->GetTcpServer()->SetAcceptSocketCount(Count);
    }
}

void UHPTcpServerManager::SetSocketBufferSize(const int64& SocketBufferSize)
{
    if (TcpServerListener.IsValid())
    {
        const DWORD BufferSize = UKismetMathLibrary::Clamp(SocketBufferSize,0,SocketBufferSize);
        TcpServerListener.Get()->GetTcpServer()->SetAcceptSocketCount(BufferSize);
    }
}

void UHPTcpServerManager::SetSocketListenQueue(const int64& SocketListenQueue)
{
    if (TcpServerListener.IsValid())
    {
        const DWORD ListenQueue = UKismetMathLibrary::Clamp(SocketListenQueue,0,SocketListenQueue);
        TcpServerListener.Get()->GetTcpServer()->SetAcceptSocketCount(ListenQueue);
    }
}

bool UHPTcpServerManager::DisconnectLongConnections(int64 Period, bool bForce)
{
    if (TcpServerListener.IsValid())
    {
        return   TcpServerListener.Get()->GetTcpServer()->DisconnectLongConnections(Period,bForce);
    }
    
    return false;
}

bool UHPTcpServerManager::DisconnectSilenceConnections(int64 Period, bool bForce)
{
    if (TcpServerListener.IsValid())
    {
        return   TcpServerListener.Get()->GetTcpServer()->DisconnectSilenceConnections(Period,bForce);
    }
    
    return false;
}

int64 UHPTcpServerManager::GetMaxPackSize() const
{
    if (!TcpServerListener.IsValid() || !TcpServerListener.Get()->GetTcpServer()->HasStarted())
        return -1;
    return TcpServerListener.Get()->GetTcpServer()->GetMaxPackSize();
}

FString UHPTcpServerManager::GetListenAddress(bool bAppendPort) const
{
    if(!TcpServerListener.IsValid() || !HasStarted())
        return TEXT("");

    TCHAR Address[50];
    int32 AddressLen = 50;
    uint16 Port = -1;
    if(!TcpServerListener.Get()->GetTcpServer()->GetListenAddress(Address,AddressLen,Port))
        return TEXT("");
    
    return bAppendPort ? FString(Address) + TEXT(":") + FString::FromInt(Port) : FString(Address);
}

bool UHPTcpServerManager::GetConnectionLocalAddress(const int64& ConnectionID, FString& OutAddress, bool bAppendPort)const
{
    if(!TcpServerListener.IsValid() || !HasStarted())
        return false;
    
    TCHAR Address[50];
    int32 AddressLen = 50;
    uint16 Port = -1;
    
    if(!TcpServerListener.Get()->GetTcpServer()->GetLocalAddress(ConnectionID,Address,AddressLen,Port))
        return false;

    OutAddress = bAppendPort ? FString(Address) + TEXT(":") + FString::FromInt(Port) : FString(Address) ;
    return true;
}

bool UHPTcpServerManager::GetConnectionRemoteAddress(const int64& ConnectionID, FString& OutAddress, bool bAppendPort)const
{
    if(!TcpServerListener.IsValid() || !HasStarted())
        return false;
    
    TCHAR Address[50];
    int32 AddressLen = 50;
    uint16 Port = -1;
    
    if(!TcpServerListener.Get()->GetTcpServer()->GetRemoteAddress(ConnectionID,Address,AddressLen,Port))
        return false;

    OutAddress = bAppendPort ? FString(Address) + TEXT(":") + FString::FromInt(Port) : FString(Address) ;
    return true;
}

ETcpServerState UHPTcpServerManager::GetTcpServerState() const
{
    if(!TcpServerListener.IsValid())
        return ETcpServerState::NOTREADY;

    EnServiceState ServiceState = TcpServerListener.Get()->GetTcpServer()->GetState();
    return static_cast<ETcpServerState>(ServiceState);
}

bool UHPTcpServerManager::GetConnectionPeriod(const int64& ConnectionID, FString& Period) const
{
    if(!TcpServerListener.IsValid() || !IsConnected(ConnectionID))
        return false;
    DWORD ConnectionPeriod = 0;
    const bool Result = TcpServerListener.Get()->GetTcpServer()->GetConnectPeriod(ConnectionID,ConnectionPeriod);
    Period = FString::Printf(TEXT("%lu"),ConnectionPeriod);
    return Result;
}

bool UHPTcpServerManager::GetConnectionSilencePeriod(const int64& ConnectionID, FString& Period) const
{
    if(!TcpServerListener.IsValid() || !IsConnected(ConnectionID))
        return false;
    DWORD ConnectionPeriod = 0;
    const bool Result = TcpServerListener.Get()->GetTcpServer()->GetSilencePeriod(ConnectionID,ConnectionPeriod);
    Period = FString::Printf(TEXT("%lu"),ConnectionPeriod);
    return Result;
}

int64 UHPTcpServerManager::GetMaxConnectionCount() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return  TcpServerListener.Get()->GetTcpServer()->GetMaxConnectionCount();
}

bool UHPTcpServerManager::IsMarkSilence() const
{
    if(!TcpServerListener.IsValid())
        return false;
    return TcpServerListener.Get()->GetTcpServer()->IsMarkSilence();
}

int64 UHPTcpServerManager::GetKeepAliveTime() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return TcpServerListener.Get()->GetTcpServer()->GetKeepAliveTime();
}

int64 UHPTcpServerManager::GetKeepAliveTimeInterval() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return TcpServerListener.Get()->GetTcpServer()->GetKeepAliveInterval();
}

int64 UHPTcpServerManager::GetAcceptSocketCount() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return TcpServerListener.Get()->GetTcpServer()->GetAcceptSocketCount();
}

int64 UHPTcpServerManager::GetSocketBufferSize() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return TcpServerListener.Get()->GetTcpServer()->GetSocketBufferSize();
}

int64 UHPTcpServerManager::GetSocketListenQueue() const
{
    if(!TcpServerListener.IsValid())
        return  -1;
    return TcpServerListener.Get()->GetTcpServer()->GetSocketListenQueue();
}

bool UHPTcpServerManager::GetConnectPeriod(const int64& ConnectionID, int64& Period) const
{
    if(!TcpServerListener.IsValid())
        return  false;

    DWORD OutPeriod = -1;
    const bool Result = TcpServerListener.Get()->GetTcpServer()->GetConnectPeriod(ConnectionID,OutPeriod);
    
    Period = static_cast<int64>(OutPeriod / 1000);
    return Result;
}

bool UHPTcpServerManager::GetSilencePeriod(const int64& ConnectionID, int64& SilencePeriod) const
{
    if(!TcpServerListener.IsValid())
        return  false;

    DWORD OutPeriod = -1;
    const bool Result = TcpServerListener.Get()->GetTcpServer()->GetSilencePeriod(ConnectionID,OutPeriod);
    SilencePeriod = static_cast<int64>(OutPeriod / 1000);
    return Result;
}

bool UHPTcpServerManager::GetLocalAddress(const int64& ConnectionID, FString& IPAddress, bool AppendPort) const
{
    if(!TcpServerListener.IsValid())
        return false;

    TCHAR OutAddress[50];
    int32 AddressLength = 50;
    uint16 OutPort = 0;
    if(TcpServerListener.Get()->GetTcpServer()->GetLocalAddress(ConnectionID,OutAddress,AddressLength,OutPort))
    {
        IPAddress = AppendPort ? FString(OutAddress) + TEXT(":") + FString::FromInt(OutPort) : FString(OutAddress);
        return true;
    }
    
    return false;
}

bool UHPTcpServerManager::GetRemoteAddress(const int64& ConnectionID, FString& IPAddress, bool AppendPort) const
{
    if(!TcpServerListener.IsValid())
        return false;

    TCHAR OutAddress[50];
    int32 AddressLength = 50;
    uint16 OutPort = 0;
    if(TcpServerListener.Get()->GetTcpServer()->GetRemoteAddress(ConnectionID,OutAddress,AddressLength,OutPort))
    {
        IPAddress = AppendPort ? FString(OutAddress) + TEXT(":") + FString::FromInt(OutPort) : FString(OutAddress);
        return true;
    }
    
    return false;
}

bool UHPTcpServerManager::GetPendingDataLength(const int64& ConnectionID, int32& Pending) const
{
    if(!TcpServerListener.IsValid())
        return false;

    return TcpServerListener.Get()->GetTcpServer()->GetPendingDataLength(ConnectionID,Pending);
}

FString UHPTcpServerManager::BytesToString(const TArray<uint8>& Bytes)
{
    if(Bytes.Num() <= 0) return TEXT("");

    FString Result;
    FFileHelper::BufferToString(Result,Bytes.GetData(),Bytes.Num());
    return Result;
}

void UHPTcpServerManager::StringToBytes(const FString& String, TArray<uint8>& Bytes)
{
    if(String.IsEmpty()) return;

    const std::string TempString = TCHAR_TO_UTF8(*String);
    Bytes.SetNumUninitialized(TempString.length());

    FMemory::Memcpy(Bytes.GetData(),TempString.c_str(),TempString.length());
    
    Bytes.Push(TEXT('\0'));
}

void UHPTcpServerManager::SetIsSending(bool SendState)
{
    if(bIsSending == SendState) return;
    bIsSending = SendState;
}

void UHPTcpServerManager::BindServerDelegate(const FTcpServerStartOptions &Options)
{ 
    if(!TcpServerListener.IsValid()) return;

    TcpServerStartOptions = MoveTemp(const_cast<FTcpServerStartOptions&>(Options));
    
    TcpServerListener.Get()->OnConnectionAcceptedDelegate().BindWeakLambda(this,[this](const int64 ConnID)
    {
        AsyncTask(ENamedThreads::GameThread, [this, ConnID] { this->TcpServerStartOptions.OnAccept.ExecuteIfBound(ConnID); });
    });


    TcpServerListener.Get()->OnServerReceiveMessageDelegate().BindWeakLambda(this,[this](const int64 ConnID, const FString Message)
    {
        AsyncTask(ENamedThreads::GameThread, [this, ConnID, Message] { this->TcpServerStartOptions.OnReceiveMessage.ExecuteIfBound(ConnID, Message); });
    });

    TcpServerListener.Get()->OnServerReceiveBytesDelegate().BindWeakLambda(this,[this](const int64 ConnID,const TArray<uint8>& Bytes)
    {
        AsyncTask(ENamedThreads::GameThread, [this, ConnID, Bytes] { this->TcpServerStartOptions.OnReceiveBytes.ExecuteIfBound(ConnID, Bytes); });
    });
    
    TcpServerListener.Get()->OnServerCloseDelegate().BindWeakLambda(this,[this](const int64 ConnID, const int32 ErrorCode)
    {     
        AsyncTask(ENamedThreads::GameThread, [this, ConnID, ErrorCode] { this->TcpServerStartOptions.OnClose.ExecuteIfBound(ConnID, ErrorCode); });
    });

    TcpServerListener.Get()->OnShotDownDelegate().BindWeakLambda(this, [this]()
    {    
        AsyncTask(ENamedThreads::GameThread, [this] { this->TcpServerStartOptions.OnShotDown.ExecuteIfBound(); });
    });

}

void UHPTcpServerManager::ApplyTcpServerSettings()
{
    if (TcpServerListener.IsValid())
    {
        const UHPTcpServerSettings* TcpServerSettings = GetDefault<UHPTcpServerSettings>();

        if (TcpServerSettings != nullptr)
        {
            TcpServerListener.Get()->GetTcpServer()->SetKeepAliveInterval(TcpServerSettings->GetAbnormalAliveTimeInterval());

            TcpServerListener.Get()->GetTcpServer()->SetKeepAliveTime(TcpServerSettings->GetKeepAliveTimeInterval());

            TcpServerListener.Get()->GetTcpServer()->SetAcceptSocketCount(TcpServerSettings->GetAcceptSocketCount());

            TcpServerListener.Get()->GetTcpServer()->SetSocketBufferSize(TcpServerSettings->GetSocketBufferSize());

            TcpServerListener.Get()->GetTcpServer()->SetSocketListenQueue(TcpServerSettings->GetSocketListenQueue());

            TcpServerListener.Get()->GetTcpServer()->SetMaxPackSize(TcpServerSettings->GetMaxPackSize());

            TcpServerListener.Get()->GetTcpServer()->SetMaxConnectionCount(TcpServerSettings->GetMaxConnectionCount());

            TcpServerListener.Get()->GetTcpServer()->SetMarkSilence(TcpServerSettings->GetIsMarkSilence());
        }
        
    }
}


