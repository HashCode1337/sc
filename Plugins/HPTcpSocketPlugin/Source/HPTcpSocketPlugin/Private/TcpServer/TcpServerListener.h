// Copyright 2020 byTangs, All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "HAL/Runnable.h"

#include "windows/AllowWindowsPlatformTypes.h"
#include "SocketInterface.h"
#include "HPTypeDef.h"




/**
 *  Delegate type for accepted TCP connections.
 *  The  parameter is the unique  id of connection
 */
DECLARE_DELEGATE_OneParam(FOnConnectionAcceptedDelegate,const int64);


/**
 *  Delegate type for receive .
 *  The first parameter is the unique  id of connection
 *  The second parameter is the received message
 */
DECLARE_DELEGATE_TwoParams(FOnServerReceiveMessageDelegate, const int64,const FString);


/**
 *  Delegate type for ShotDown
 */
DECLARE_DELEGATE(FOnShotDownDelegate);


/**
 *  Delegate type for Close .
 *  The first parameter is the unique  id of connection
 *  The second parameter is the error code
 */
DECLARE_DELEGATE_TwoParams(FOnServerCloseDelegate, const int64, const int32);

DECLARE_DELEGATE_TwoParams(FOnServerReceiveBytesDelegate, const int64, const TArray<uint8>&);



class FTcpServerListener : public FRunnable,public ITcpServerListener
{
public:
	
	FTcpServerListener();

	~FTcpServerListener();


public:

	//---------------------Runnable Thread Interface ----------------------//
	virtual bool Init()  override;

	
	virtual uint32 Run() override;

	
	virtual void Stop()  override { bStopping = true; }


	virtual void Exit()  override { }

public:

	//---------------------ITcpServerListener  Interface ----------------------//
	virtual EnHandleResult OnPrepareListen(ITcpServer* Sender, SOCKET Listen)									  override { return HR_IGNORE; }


	virtual EnHandleResult OnAccept(ITcpServer* Sender, CONNID ConnID, UINT_PTR Client)							  override;
	 

	virtual EnHandleResult OnHandShake(ITcpServer* Sender, CONNID ConnID)										  override { return HR_IGNORE; }


	virtual EnHandleResult OnReceive(ITcpServer* Sender, CONNID ConnID, int Length)								  override { return HR_IGNORE; }


	virtual EnHandleResult OnReceive(ITcpServer* Sender, CONNID ConnID, const BYTE* Data, int Length)			  override;

	
	virtual EnHandleResult OnSend(ITcpServer* Sender, CONNID ConnID, const BYTE* pData, int Length)				  override { return HR_IGNORE; }


	virtual EnHandleResult OnShutdown(ITcpServer* Sender)														  override;


	virtual EnHandleResult OnClose(ITcpServer* Sender, CONNID ConnID, EnSocketOperation Operation, int ErrorCode) override;



public:

	/**
	 * Gets a delegate to be invoked when an incoming connection has been accepted.
	 * 
	 */
	FOnConnectionAcceptedDelegate& OnConnectionAcceptedDelegate()
	{
		return ConnectionAcceptedDelegate;
	}
	
	FOnServerReceiveMessageDelegate& OnServerReceiveMessageDelegate()
	{
		return ReceiveMessageDelegate;
	}

	FOnServerReceiveBytesDelegate& OnServerReceiveBytesDelegate()
	{
		return ReceiveBytesDelegate;
	}
	
	FOnShotDownDelegate& OnShotDownDelegate()
	{
		return ShotDownDelegate;
	}

	FOnServerCloseDelegate& OnServerCloseDelegate()
	{
		return CloseDelegate;
	}
	
	/**
	 *  Get Tcp Pack Server Pointer
	 */
	ITcpPackServer* GetTcpServer() const 
	{
		return TcpPackServer;
	}
	
private:

	/** TcpPackServer */
	ITcpPackServer*		TcpPackServer = nullptr;

	/** Runnable Thread */
	FRunnableThread*    Thread		  = nullptr;

	/** Loop */
	bool				bStopping;

	/** Holds a delegate to be invoked when an incoming connection has been accepted. */
	FOnConnectionAcceptedDelegate ConnectionAcceptedDelegate;

	/** Receive Message Delegate */
	FOnServerReceiveMessageDelegate ReceiveMessageDelegate;

	/** Receive bytes Delegate */
	FOnServerReceiveBytesDelegate ReceiveBytesDelegate;

	/** ShotDown Delegate */
	FOnShotDownDelegate ShotDownDelegate;

	/** Close Delegate */
	FOnServerCloseDelegate CloseDelegate;

	/** Server Receive Matcher For Multi Client */
	TMap<CONNID,TSharedPtr<class FServerReceiveMatcher>> ServerReceiveMatcher;
	
};



class FServerReceiveMatcher
{
public:
	
	FServerReceiveMatcher(const CONNID InConnectionID,FTcpServerListener* InTcpServerListener);

	~FServerReceiveMatcher();
	
	FServerReceiveMatcher(const FServerReceiveMatcher&) = delete;

	FServerReceiveMatcher& operator = (const FServerReceiveMatcher&) = delete;
	
	EnHandleResult HandleReceiveData(const TArray<uint8>& Data, int Length);


private:

	CONNID 			       ConnectionID;
	
	FTcpServerListener*    TcpServerListener;
	
	/** receive bytes*/
	TArray<uint8> Bytes;

	/** Bytes Length*/
	int64 BytesLength = 0;

	/** Current Byte Read*/
	int64 ByteRead = 0;

	/** Data Type */
	FString DataType;

	/** Receive File Name */
	FString Filename;

};
