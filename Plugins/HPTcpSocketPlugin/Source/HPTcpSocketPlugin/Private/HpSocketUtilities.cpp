// Copyright 2020 byTangs, All Rights Reserved.
#include "HpSocketUtilities.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "HPTypeDef.h"
#include "HPSocket.h"
#include "Windows/HideWindowsPlatformTypes.h"

bool GetHostNameInternal(FString& OutHostName)
{
#if PLATFORM_WINDOWS
    //using Win API To Get Host Name
    char HostNameTemp[128];
    if (gethostname(HostNameTemp, 128) == 0)
    {
        OutHostName = FString(HostNameTemp);
        return  true;
    }
    HP_LOG(Error, TEXT("Get Host Name Failed ,Error Code %d"), SYS_GetLastError());
    return false;
#endif
    return false;
}

bool GetLocalAddressInternal(const EIPAddressType& IPAddressType, TArray<FString>& OutAddress)
{
    FString HostName;
    if (!GetHostNameInternal(HostName))
        return false;

    LPTIPAddr* OutAddressPtr = nullptr;
    int32      Count = 0;
    const EnIPAddrType IPAddressTypeTemp = EnIPAddrType(static_cast<int>(IPAddressType));
    if (!SYS_EnumHostIPAddresses(*HostName, IPAddressTypeTemp, &OutAddressPtr, Count))
        return false;

    for (int i = 0; i < Count; ++i)
    {
        OutAddress.Add(FString((*OutAddressPtr + i)->address));       
    }
    SYS_FreeHostIPAddresses(OutAddressPtr);
    return true;
}


bool IPIsValidInternal(const FString& InAddress)
{
    if (InAddress.IsEmpty()) return false;
	
    return SYS_IsIPAddress(*InAddress);
}


