// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCGameMode.h"
#include "SCHUD.h"
#include "SCCharacter.h"
#include "Core/SC_PlayerState.h"
#include "UObject/ConstructorHelpers.h"

ASCGameMode::ASCGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASCHUD::StaticClass();
}

void ASCGameMode::StartPlay()
{
	Super::StartPlay();
}

void ASCGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASCGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}
