// Fill out your copyright notice in the Description page of Project Settings.


#include "SCBaseWeapon.h"

#include "SCCharacter.h"
#include "SCProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Net/UnrealNetwork.h"

void ASCBaseWeapon::ExecFire(bool fire)
{
	bool bIsServer = GetOwner()->HasAuthority();
	bool bIsLocal = GetInstigator()->IsLocallyControlled();
	bIsFiring = fire;
}

void ASCBaseWeapon::S_TryFire_Implementation()
{
	
	if (!WeaponMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("ASCBaseWeapon::S_TryFire_Implementation() - WeaponMesh is nullptr"));
		return;
	}

	GEngine->AddOnScreenDebugMessage(0, 10, FColor::Red, TEXT("Income"));
	
	N_TryFire();

	FTransform BulletStartPos = WeaponMesh->GetSocketTransform(WeaponSocketsData.BulletSpawnSocketName, RTS_World);
	FVector SpLoc = BulletStartPos.GetLocation();
	FRotator SpRot = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(BulletStartPos.GetRotation().Rotator().Vector(), Spread).Rotation();

	FActorSpawnParameters SpawnParams;

	SpawnParams.Instigator = this->GetInstigator();
	SpawnParams.Owner = this->GetOwner();

	GetWorld()->SpawnActor(LoadedBulletType, &SpLoc, &SpRot, SpawnParams);
}

void ASCBaseWeapon::N_TryFire_Implementation()
{
	// Prevent local fire, because it has been executed before RPC on local pc
	if (!IsValid(GetInstigator())) return;

	bool bIsLocal = GetInstigator()->IsLocallyControlled();
	if (bIsLocal) return;
	PlayFireFX();
}

void ASCBaseWeapon::PlayFireFX()
{
	if (ShootSoundEmitter) ShootSoundEmitter->Play(0.0f);
	if (MuzzleFlashFXSystem) MuzzleFlashFXSystem->ActivateSystem(true);
	PlayFireAnims();
}

void ASCBaseWeapon::PlayFireAnims()
{
	if (WeaponOwner)
	{
		ASCCharacter* WeaponOwnerChar = Cast<ASCCharacter>(WeaponOwner);
		if (WeaponOwnerChar)
		{
			WeaponOwnerChar->GetMesh1P()->GetAnimInstance()->Montage_Play(ShootAnimMontage.HandsAM);
		}
	}

	if (WeaponMesh)
	{
		WeaponMesh->GetAnimInstance()->Montage_Play(ShootAnimMontage.WeaponAM);
	}

	//ToDo Third Person
}

void ASCBaseWeapon::PlayReloadAnims()
{
	if (WeaponOwner)
	{
		ASCCharacter* WeaponOwnerChar = Cast<ASCCharacter>(WeaponOwner);
		if (WeaponOwnerChar)
		{
			WeaponOwnerChar->GetMesh1P()->GetAnimInstance()->Montage_Play(ReloadAnimMontage.HandsAM);
		}
	}

	if (WeaponMesh)
	{
		WeaponMesh->GetAnimInstance()->Montage_Play(ReloadAnimMontage.WeaponAM);
		
	}

	//ToDo Third Person
}

void ASCBaseWeapon::InitWeapon()
{
	if (ShootSoundEmitter)
	{
		ShootSoundEmitter->SetSound(WeaponEffectsData.BulletFireSound);
		ShootSoundEmitter->AttenuationSettings = WeaponEffectsData.ShootAttent;
	}

	if (MuzzleFlashFXSystem)
	{
		MuzzleFlashFXSystem->SetTemplate(WeaponEffectsData.ShootVFX);
	}
}

// Sets default values
ASCBaseWeapon::ASCBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
	
	MuzzleFlashFXSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MuzzleFlashFX"));
	MuzzleFlashFXSystem->SetupAttachment(WeaponMesh, WeaponSocketsData.MuzzleFlashSocketName);

	ShootSoundEmitter = CreateDefaultSubobject<UAudioComponent>(TEXT("ShootSound"));
	ShootSoundEmitter->SetupAttachment(RootComponent);
}

void ASCBaseWeapon::HideWeaponItems()
{
	if (WeaponMesh)
	{
		WeaponMesh->HideBoneByName(WeaponSocketsData.GrenadeLauncherSocketName, EPhysBodyOp::PBO_MAX);
		WeaponMesh->HideBoneByName(WeaponSocketsData.SilencerSocketName, EPhysBodyOp::PBO_MAX);
		WeaponMesh->HideBoneByName(WeaponSocketsData.ScopeSocketName, EPhysBodyOp::PBO_MAX);
	}
}

void ASCBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	InitWeapon();
}

void ASCBaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsFiring)
	{
		if (GetWorld()->GetRealTimeSeconds() > lastFire + 60.f / RPM)
		{
			lastFire = GetWorld()->GetRealTimeSeconds();
			S_TryFire();
			PlayFireFX();
		}
	}
}

void ASCBaseWeapon::OnRep_bIsFiring(bool fire)
{
	
}

void ASCBaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(ASCBaseWeapon, bIsFiring, COND_None)
}