// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SCBaseWeapon.generated.h"

class ASCProjectile;
class UParticleSystemComponent;
class UAudioComponent;

#pragma region Structures

USTRUCT(BlueprintType)
struct FWeaponAnimPairs 
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* HandsAM = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* WeaponAM = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* ThirdPersonAM = nullptr;

};

USTRUCT(BlueprintType)
struct FWeaponSocketsData
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName GrenadeLauncherSocketName = TEXT("wpn_launcher");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName SilencerSocketName = TEXT("wpn_silencer");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName ScopeSocketName = TEXT("wpn_scope");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName MuzzleFlashSocketName = TEXT("muzzle");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName BulletSpawnSocketName = TEXT("bulletSocket");
};

USTRUCT(BlueprintType)
struct FWeaponEffectsData
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundBase* BulletFireSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundAttenuation* ShootAttent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UParticleSystem* ShootVFX = nullptr;
};

#pragma endregion Structures

UCLASS(Blueprintable)
class SC_API ASCBaseWeapon : public AActor
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
	AActor* WeaponOwner = nullptr;

private:

#pragma region MainExposed
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FWeaponSocketsData WeaponSocketsData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FWeaponEffectsData WeaponEffectsData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FWeaponAnimPairs ShootAnimMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FWeaponAnimPairs ReloadAnimMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	UAudioComponent* ShootSoundEmitter = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	UParticleSystemComponent* MuzzleFlashFXSystem = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float RPM = 300;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true, ClampMin = 0, ClampMax = 10))
	float Spread = 2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<ASCProjectile> LoadedBulletType;

#pragma endregion MainExposed

#pragma region Core
	
	UPROPERTY(ReplicatedUsing = OnRep_bIsFiring, EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	bool bIsFiring = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float lastFire = 0.f;

#pragma endregion Core

public:
	UFUNCTION(BlueprintCallable)
	void ExecFire(bool fire);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void S_TryFire();
	
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void N_TryFire();

	UFUNCTION()
	void OnRep_bIsFiring(bool fire);
	
	UFUNCTION(BlueprintCallable)
	void PlayFireFX();

	UFUNCTION(BlueprintCallable)
	void PlayFireAnims();

	UFUNCTION(BlueprintCallable)
	void PlayReloadAnims();
	
	UFUNCTION(BlueprintCallable)
	void InitWeapon();


public:	
	// Sets default values for this actor's properties
	ASCBaseWeapon();

	UFUNCTION(BlueprintCallable)
	void HideWeaponItems();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* WeaponMesh = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
