// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "SC_CommonDataModels.h"
#include "SC_GameInstance.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SC_API USC_GameInstance : public UGameInstance
{
	GENERATED_BODY()

		~USC_GameInstance();

public:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UDataTable* DTSettings = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FString NickName = "Player";

	UPROPERTY(BlueprintReadOnly)
	FUserSelfData UserData;

	UPROPERTY(BlueprintReadOnly)
	bool PlayerLogged = false;

	UFUNCTION(BlueprintCallable)
	void SetNickName(FString NewNickName);

	UFUNCTION()
	void SetUserSelfData(FString BackendAnswer);

protected:

	virtual void Shutdown() override;

private:

	void SendPlayerExit();
};
