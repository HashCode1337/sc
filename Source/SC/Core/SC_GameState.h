// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/GameState.h"
#include "SC_GameState.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASC_GameState : public AGameStateBase
{
	GENERATED_BODY()

	ASC_GameState(const FObjectInitializer& ObjectInitializer);

public:

	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite)
	float SkySphereRotation;
	
};
