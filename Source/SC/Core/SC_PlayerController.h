// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SC_PlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRespawn);

/**
 * 
 */
UCLASS()
class SC_API ASC_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	FString GetMyName();

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void ApplyNickName(const FString& Name);

	UFUNCTION(BlueprintCallable)
	void Respawn();

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FRespawn OnRespawn;

public:
	virtual void BeginPlay() override;
	
	
};
