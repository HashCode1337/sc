// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_StaticLibrary.h"
#include "Containers/UnrealString.h"
#include "Misc/FileHelper.h"
#include "Kismet/KismetStringLibrary.h"
#include "Internationalization/Regex.h"
#include "HAL/FileManager.h"
#include "JsonObjectConverter.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

#include "SC_StaticLibrary.h"

DEFINE_LOG_CATEGORY(LogConfigs);
DEFINE_LOG_CATEGORY(Logger);


//====================================================================================================//

const FString USC_StaticLibrary::BackendRegisterRequest = TEXT("/register?login=%s&email=%s&password=%s");

//====================================================================================================//


bool USC_StaticLibrary::GetDataBaseSettings(FDataBaseSettings& Settings)
{
	FString ResultString;
	FString FileName = TEXT("dbconfig.json");
	FString Path = FPaths::ProjectDir() + FileName;

	if (!IFileManager::Get().FileExists(*Path))
	{

		UE_LOG(LogConfig, Fatal, TEXT("ERROR: dbconfig.json not found!!!"))
		return false;
	}

	FFileHelper::LoadFileToString(ResultString, *(FPaths::ProjectDir() + FileName));

	FDataBaseSettings Config;
	FJsonObjectConverter::JsonObjectStringToUStruct(ResultString, &Config);

	Settings = Config;

	return true;
}

bool USC_StaticLibrary::GetSettings(FGameConfig& Settings)
{
	FString ResultString;
	FString FileName = TEXT("config.json");
	FString Path = FPaths::ProjectDir() + FileName;

	if (!IFileManager::Get().FileExists(*Path))
	{

		UE_LOG(LogConfig, Fatal, TEXT("ERROR: config.json not found!!!"))
			return false;
	}

	FFileHelper::LoadFileToString(ResultString, *(FPaths::ProjectDir() + FileName));

	FGameConfig DBSettings;
	FJsonObjectConverter::JsonObjectStringToUStruct(ResultString, &DBSettings);

	Settings = DBSettings;

	return true;
}

FString USC_StaticLibrary::GetGlobalParseSymbol()
{
	return ";";
}

FString USC_StaticLibrary::GetNicknameRgxPattern()
{
	FString Pattern = TEXT("^[A-Za-z0-9]{4,20}$");
	return Pattern;
}

FString USC_StaticLibrary::GetPasswordRgxPattern()
{
	FString Pattern = TEXT("^[A-Za-z0-9]{4,20}$");
	return Pattern;
}

FString USC_StaticLibrary::GetEmailRgxPattern()
{
	//FString Pattern = TEXT("^[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$");
	FString Pattern = TEXT("^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$");
	return Pattern;
}

void USC_StaticLibrary::ParseCredentials(const FString& InputString, FString& Login, FString& Pwd)
{
	TArray<FString> ParsedArray;
	FString ParseSymbol = GetGlobalParseSymbol();
	FString Lgn;
	FString Pw;

	InputString.ParseIntoArray(ParsedArray, *ParseSymbol);

	Login = ParsedArray[1];
	Pwd = ParsedArray[2];
}

FString USC_StaticLibrary::GetVersion()
{
	FString Ver;

	GConfig->GetString(
		TEXT("/Script/EngineSettings.GeneralProjectSettings"),
		TEXT("ProjectVersion"),
		Ver,
		GGameIni
	);

	return Ver;
}

void USC_StaticLibrary::CombineLogin(const FString& Login, const FString& Pwd, FString& ResultMessage)
{
	FString StrEnum = UKismetStringLibrary::Conv_IntToString((int)ELoginType::Login);
	FString ParseSymbol = GetGlobalParseSymbol();
	ResultMessage = StrEnum + ParseSymbol + Login + ParseSymbol + Pwd;
}

void USC_StaticLibrary::CombineRegister(const FString& Login, const FString& Pwd, FString& ResultMessage)
{
	FString StrEnum = UKismetStringLibrary::Conv_IntToString((int)ELoginType::Register);
	FString ParseSymbol = GetGlobalParseSymbol();
	ResultMessage = StrEnum + ParseSymbol + Login + ParseSymbol + Pwd;
}

ELoginType USC_StaticLibrary::RecognizeMsg(FString Message)
{
	FString LeftMsg;

	Message.Split(USC_StaticLibrary::GetGlobalParseSymbol(), &LeftMsg, nullptr, ESearchCase::IgnoreCase, ESearchDir::FromStart);

	int32 ans = UKismetStringLibrary::Conv_StringToInt(LeftMsg);
	ELoginType CastedAnswer = (ELoginType)ans;

	switch (CastedAnswer)
	{
	case ELoginType::Login:
		break;
	case ELoginType::Register:
		break;
	default:
		USC_StaticLibrary::PrintMsg(0, FColor::Red, "ERROR: USC_StaticLibrary::RecognizeMsg(), switch case not implemented yet");
		break;
	}

	return CastedAnswer;
}

bool USC_StaticLibrary::GetBoolFromParsed(const FString& InputStr, int ParamIndex)
{
	TArray<FString> ResultArr;
	InputStr.ParseIntoArray(ResultArr, *GetGlobalParseSymbol());

	if (ResultArr.IsValidIndex(ParamIndex))
	{
		FString Res = ResultArr[ParamIndex];
		bool ConvertedRes = (bool)UKismetStringLibrary::Conv_StringToInt(Res);

		if (ConvertedRes)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

void USC_StaticLibrary::PrintMsg(int key, FColor Color, FString Msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(key, 5, Color, Msg);
	}
}

void USC_StaticLibrary::Log(ELogType SCLogType, FString Msg)
{
	switch (SCLogType)
	{
	case ELogType::Log:
		UE_LOG(Logger, Log, TEXT("%s"), *Msg);
		break;
	case ELogType::Warn:
		UE_LOG(Logger, Warning, TEXT("%s"), *Msg);
		break;
	case ELogType::Error:
		UE_LOG(Logger, Error, TEXT("%s"), *Msg);
		break;
	case ELogType::Fatal:
		UE_LOG(Logger, Fatal, TEXT("%s"), *Msg);
		break;
	default:
		break;
	}

}

bool USC_StaticLibrary::RegexMatch(const FString& Rules, const FString& Arg)
{
	const FRegexPattern Ptrn = FRegexPattern(Rules);
	FRegexMatcher Mtchs = FRegexMatcher(Ptrn, Arg);
	
	bool find = Mtchs.FindNext();

	return find;
}

void USC_StaticLibrary::MakeTestHttpRequest()
{
	FGameConfig GC;
	bool IsConfigExist = GetSettings(GC);

	if (!IsConfigExist)
	{
		return;
	}

	FString Url = GC.Backend + TEXT("/?name=tom&pwd=pidr");

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("GET"));
	pRequest->SetURL(Url);

	pRequest->OnProcessRequestComplete().BindLambda(
		[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully) 
		{

			if (connectedSuccessfully) {
				auto resp = pRequest->GetResponse();
				resp->GetContentAsString();
				auto zaebal = resp->GetContentAsString();

				PrintMsg(0, FColor::Red, *zaebal);
			}
			else
			{
				switch (pRequest->GetStatus()) {
				case EHttpRequestStatus::Failed_ConnectionError:
					UE_LOG(LogTemp, Error, TEXT("Connection failed."));
				default:
					UE_LOG(LogTemp, Error, TEXT("Request failed."));
				}
			}
		});

	pRequest->ProcessRequest();
}
