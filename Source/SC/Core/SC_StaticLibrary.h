// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include <SC/Core/SC_CommonDataModels.h>
#include "Misc/DateTime.h"
#include "SC_StaticLibrary.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogConfigs, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Logger, Log, All);

UCLASS()
class SC_API USC_StaticLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	// Backend
	//====================================================================================================//

	/*1. Login
	2. Email
	3. Password*/
	static const FString BackendRegisterRequest;

	//====================================================================================================//

public:

	// Static Settings
	//====================================================================================================//

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetGlobalParseSymbol();

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetNicknameRgxPattern();

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetPasswordRgxPattern();

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetEmailRgxPattern();

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "DB Config Load"))
	static bool GetDataBaseSettings(FDataBaseSettings& Settings);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Config Main Settings"))
	static bool GetSettings(FGameConfig& Settings);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Project Version"))
	static FString GetVersion();

	// Functions
	//====================================================================================================//
	
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Credentials"))
	static void ParseCredentials(const FString& InputString, FString& Login, FString& Pwd);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Combine Login"))
	static void CombineLogin(const FString& Login, const FString& Pwd, FString& ResultMessage);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Combine Login"))
	static void CombineRegister(const FString& Login, const FString& Pwd, FString& ResultMessage);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Recognize"))
	static ELoginType RecognizeMsg(FString Message);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse"))
	static bool GetBoolFromParsed(const FString& InputStr, int ParamIndex);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static bool RegexMatch(const FString& Rules, const FString& Arg);

	// Util
	//====================================================================================================//

	UFUNCTION(BlueprintCallable)
	static void PrintMsg(int key, FColor Color, FString Msg);


	UFUNCTION(BlueprintCallable)
	static void Log(ELogType SCLogType, FString Msg);

	// Testing
	//====================================================================================================//

	UFUNCTION(BlueprintCallable)
	static void MakeTestHttpRequest();
	
};
