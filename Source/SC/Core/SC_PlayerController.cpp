// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PlayerController.h"
#include "SC_GameInstance.h"
#include "SC_PlayerState.h"

FString ASC_PlayerController::GetMyName()
{
	USC_GameInstance* GI = Cast<USC_GameInstance>(GetGameInstance());

	if (GI)
	{
		return GI->NickName;
	}
	else
	{
		return "ERROR";
	}

	
}

void ASC_PlayerController::ApplyNickName_Implementation(const FString& Name)
{
	auto PS = GetPlayerState<ASC_PlayerState>();
	PS->SetNickAfterConnect(Name);
}

void ASC_PlayerController::Respawn()
{
	OnRespawn.Broadcast();
}

void ASC_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		ApplyNickName_Implementation(GetMyName());
		//GEngine->AddOnScreenDebugMessage(2, 10, FColor::Purple, FString::Printf(TEXT("SERVER: %s"), *GetMyName()));
	}
	else
	{
		ApplyNickName(GetMyName());
		//GEngine->AddOnScreenDebugMessage(2, 10, FColor::Purple, FString::Printf(TEXT("CLIENT: %s"), *GetMyName()));
	}
	
}