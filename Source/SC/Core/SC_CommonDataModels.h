// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameFramework/Character.h"
#include "../Service/MySQLBPLibrary.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SC_CommonDataModels.generated.h"

// DataBase and relative
//===============================================================================//

USTRUCT(BlueprintType)
struct FDataBaseSettings
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite)
		FString IP;
	UPROPERTY(BlueprintReadWrite)
		FString DataBaseName;
	UPROPERTY(BlueprintReadWrite)
		FString UserId;
	UPROPERTY(BlueprintReadWrite)
		FString Password;
};

USTRUCT(BlueprintType)
struct FGameConfig
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite)
		FString Backend;
};

USTRUCT(BlueprintType)
struct FUserSelfData
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadOnly)
	int64 id;
	UPROPERTY(BlueprintReadOnly)
	FString session;
	UPROPERTY(BlueprintReadOnly)
	FString guid;
	UPROPERTY(BlueprintReadOnly)
	FString account;
	UPROPERTY(BlueprintReadOnly)
	FString custom_name;
	UPROPERTY(BlueprintReadOnly)
	FString email;
	UPROPERTY(BlueprintReadOnly)
	FString password;
	UPROPERTY(BlueprintReadOnly)
	uint8 admin_level;
	UPROPERTY(BlueprintReadOnly)
	uint8 prem_level;
	UPROPERTY(BlueprintReadOnly)
	FString first_connect;
	UPROPERTY(BlueprintReadOnly)
	FString last_connect;
	UPROPERTY(BlueprintReadOnly)
	FString blocked_until;
	UPROPERTY(BlueprintReadOnly)
	int64 connects;
	UPROPERTY(BlueprintReadOnly)
	float time_played;
	UPROPERTY(BlueprintReadOnly)
	int32 kicks;
	UPROPERTY(BlueprintReadOnly)
	int32 bans;
};

USTRUCT(BlueprintType)
struct FDataBaseAccountModel : public FTableRowBase
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int64 id;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString session;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FGuid guid;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString account;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString custom_name;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString email;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString password;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	uint8 admin_level;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	uint8 prem_level;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString first_connect;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString last_connect;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString blocked_until;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int64 connects;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float time_played;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 kicks;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 bans;
};

UENUM(BlueprintType)
enum class EAnswerType : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	AccountExist = 1 UMETA(DisplayName = "AccountExist"),
	GetWholeAccount = 2 UMETA(DisplayName = "GetWholeAccount")
};

UENUM(BlueprintType)
enum class ELoginType : uint8
{
	Login = 0 UMETA(DisplayName = "Login"),
	Register = 1 UMETA(DisplayName = "Register")
};

UENUM(BlueprintType)
enum class ERegisterError : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	AlreadyExist = 1 UMETA(DisplayName = "AlreadyExist"),
	BadLogin = 2 UMETA(DisplayName = "BadLogin"),
	BadPassword = 3 UMETA(DisplayName = "BadPassword"),
	PwdNotMatch = 4 UMETA(DisplayName = "PwdNotMatch"),
	BadMail = 5 UMETA(DisplayName = "BadMail"),
	EmailAlreadyExist = 6 UMETA(DisplayName = "EmailAlreadyExist")
};

UENUM(BlueprintType)
enum class ELoginError : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	InvalidCredentials = 1 UMETA(DisplayName = "InvalidCredentials"),
	BadLogin = 2 UMETA(DisplayName = "BadLogin"),
	BadPassword = 3 UMETA(DisplayName = "BadPassword"),
	NotExist = 4 UMETA(DisplayName = "NotExist"),
	AccountNotActivated = 5 UMETA(DisplayName = "AccountNotActivated")
};

USTRUCT(BlueprintType)
struct FDataBaseAnswer : public FTableRowBase
{
	GENERATED_BODY()

	EAnswerType AnswerType = EAnswerType::Unknown;
	TArray<FMySQLDataTable> ResultsByColumn;
	TArray<FMySQLDataRow> ResultsByRow;
};

// Common
//===============================================================================//

UENUM(BlueprintType)
enum class EViewType : uint8
{
	MainMenuView = 0 UMETA(DisplayName = "MainMenuView"),
	AuthView = 1 UMETA(DisplayName = "AuthView"),
	LoggedInView = 2 UMETA(DisplayName = "LoggedInView"),
	RegisterView = 3 UMETA(DisplayName = "RegisterView")
};

UENUM(BlueprintType)
enum class ELogType : uint8
{
	Log = 0 UMETA(DisplayName = "Log"),
	Warn = 1 UMETA(DisplayName = "Warn"),
	Error = 2 UMETA(DisplayName = "Error"),
	Fatal = 3 UMETA(DisplayName = "Fatal")
};

// Backend
//===============================================================================//

USTRUCT(BlueprintType)
struct FBackendAnswer : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY()
	bool Success;
	UPROPERTY()
	FString Answer;
	UPROPERTY()
	FString Error;
};

USTRUCT(BlueprintType)
struct FMainSettings : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<class ACharacter> DefaultPlayerCharacter;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float SkySphereRotationMultiplier = 1.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString MasterServerIP = "127.0.0.1";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int MasterServerPORT = 7778;
};

//===============================================================================//

UCLASS()
class SC_API USC_CommonDataModels : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()


	
};
