// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Level.h"
#include "SC_TransitionGameMode.generated.h"

class USC_GameInstance;
UCLASS()
class SC_API ASC_TransitionGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure)
	USC_GameInstance* GetGameInstance();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UWorld* GetMainLevel();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UWorld* MainLevel;

	
};
