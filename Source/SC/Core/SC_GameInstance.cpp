// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameInstance.h"
#include "JsonObjectConverter.h"
#include "SC_StaticLibrary.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

USC_GameInstance::~USC_GameInstance()
{
	
}

void USC_GameInstance::SetNickName(FString name)
{
	NickName = name;
}

void USC_GameInstance::SetUserSelfData(FString BackendAnswer)
{
	FJsonObjectConverter::JsonObjectStringToUStruct(BackendAnswer, &UserData);
	PlayerLogged = true;
}

void USC_GameInstance::Shutdown()
{
	Super::Shutdown();

	if (PlayerLogged)
	{
		SendPlayerExit();
	}
	UE_LOG(LogTemp, Error, TEXT("Closed"));
}

void USC_GameInstance::SendPlayerExit()
{
	FGameConfig GC;
	bool IsConfigExist = USC_StaticLibrary::GetSettings(GC);

	if (!IsConfigExist)
	{
		return;
	}

	FString RequestBody = FString::Printf(TEXT("/disconnect?session=%s"), *UserData.session);
	FString Url = GC.Backend + RequestBody;

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("GET"));
	pRequest->SetURL(Url);
	pRequest->ProcessRequest();
}