// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_LoggedController.h"
#include "../Core/SC_StaticLibrary.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

#include "../Core/SC_GameInstance.h"

void USC_LoggedController::TrySetNickname(FString NewNickname)
{
	FGameConfig GC;

	USC_GameInstance* CGI = Cast<USC_GameInstance>(GI);

	FString Login = CGI->UserData.account;
	FString Pwd = CGI->UserData.password;
	FString Session = CGI->UserData.session;

	bool IsConfigExist = USC_StaticLibrary::GetSettings(GC);

	if (!IsConfigExist)
	{
		return;
	}
	//http://127.0.0.1:1337/setname?login=HashCode&password=12345&session=VP9Xw8bmUpYBaNOZ&nickname=������%20������
	FString RequestBody = FString::Printf(TEXT("/setname?login=%s&password=%s&session=%s"), *Login, *Pwd, *Session);

	FString Url = GC.Backend + RequestBody;

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("POST"));
	
	pRequest->SetURL(*Url);

	pRequest->SetHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
	pRequest->AppendToHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	
	FString ContentPOST = FString::Printf(TEXT("nickname=%s"), *NewNickname);
	pRequest->SetContentAsString(*ContentPOST);

	pRequest->OnProcessRequestComplete().BindLambda(
		[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully)
		{

			if (connectedSuccessfully) {
				auto resp = pRequest->GetResponse();
				resp->GetContentAsString();
				auto backendAnswer = resp->GetContentAsString();
				backendAnswer = resp->GetContentAsString();


				USC_StaticLibrary::Log(ELogType::Log, *backendAnswer);
				//HandleAnswer(backendAnswer);
			}
			else
			{
				switch (pRequest->GetStatus()) {
				case EHttpRequestStatus::Failed_ConnectionError:
					UE_LOG(LogTemp, Error, TEXT("Connection failed."));
				default:
					UE_LOG(LogTemp, Error, TEXT("Request failed."));
				}
			}
		});

	pRequest->ProcessRequest();
}
