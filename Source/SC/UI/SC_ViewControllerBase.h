// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Widgets/SUserWidget.h"
#include <SC/Core/SC_CommonDataModels.h>
#include "SC_ViewControllerBase.generated.h"

UCLASS(Blueprintable)
class SC_API USC_ViewControllerBase : public UObject
{
	GENERATED_BODY()

public:

	USC_ViewControllerBase();
	~USC_ViewControllerBase();

	UFUNCTION(BlueprintNativeEvent)
	void InitScreen(EViewType Type);

	virtual void HandleAnswer(FString Answer);
	
	UFUNCTION(BlueprintCallable)
	void Destroy();
	
	UPROPERTY(BlueprintReadWrite)
	UObject* ControlledWidget;

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
	UGameInstance* GI;
	
};
