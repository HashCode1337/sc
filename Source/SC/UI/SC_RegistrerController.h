// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_ViewControllerBase.h"
#include "../Core/SC_GameInstance.h"
#include "SC_RegistrerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRegisterController, Log, All)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnRegister, bool, bSuccess, ERegisterError, Error);

UCLASS()
class SC_API USC_RegistrerController : public USC_ViewControllerBase
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FOnRegister OnRegister;

	UFUNCTION(BlueprintCallable)
	void TryRegister(FString Login, FString Email, FString Password, FString Password2);

	UFUNCTION()
	void SendRequest(FString Login, FString Email, FString Password);

	virtual void HandleAnswer(FString Answer) override;
};
