// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_RegistrerController.h"
#include <SC/Core/SC_StaticLibrary.h>
#include "JsonObjectConverter.h"

#include "../Core/SC_GameInstance.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

DEFINE_LOG_CATEGORY(LogRegisterController);

void USC_RegistrerController::TryRegister(FString Login, FString Email, FString Password, FString Password2)
{

	FString NameRgxPtrn = USC_StaticLibrary::GetNicknameRgxPattern();
	FString PwdRgxPtrn = USC_StaticLibrary::GetPasswordRgxPattern();
	FString MailRgxPtrn = USC_StaticLibrary::GetEmailRgxPattern();

	if (!USC_StaticLibrary::RegexMatch(NameRgxPtrn, Login))
	{
		OnRegister.Broadcast(false, ERegisterError::BadLogin);
		return;
	}

	if (Password != Password2)
	{
		OnRegister.Broadcast(false, ERegisterError::PwdNotMatch);
		return;
	}

	if (!USC_StaticLibrary::RegexMatch(PwdRgxPtrn, Password))
	{
		OnRegister.Broadcast(false, ERegisterError::BadPassword);
		return;
	}

	if (!USC_StaticLibrary::RegexMatch(MailRgxPtrn, Email))
	{
		OnRegister.Broadcast(false, ERegisterError::BadMail);
		return;
	}

	SendRequest(Login, Email, Password);
}

void USC_RegistrerController::SendRequest(FString Login, FString Email, FString Password)
{
	FGameConfig GC;
	bool IsConfigExist = USC_StaticLibrary::GetSettings(GC);

	if (!IsConfigExist)
	{
		return;
	}

	FString RequestBody = FString::Printf(TEXT("/register?login=%s&email=%s&password=%s"), *Login, *Email, *Password);
	FString Url = GC.Backend + RequestBody;

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("GET"));
	pRequest->SetURL(Url);

	pRequest->OnProcessRequestComplete().BindLambda(
		[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully) 
		{

			if (connectedSuccessfully) {
				auto resp = pRequest->GetResponse();
				resp->GetContentAsString();
				auto backendAnswer = resp->GetContentAsString();

				HandleAnswer(backendAnswer);
			}
			else
			{
				switch (pRequest->GetStatus()) {
				case EHttpRequestStatus::Failed_ConnectionError:
					UE_LOG(LogTemp, Error, TEXT("Connection failed."));
				default:
					UE_LOG(LogTemp, Error, TEXT("Request failed."));
				}
			}
		});

	pRequest->ProcessRequest();
}

void USC_RegistrerController::HandleAnswer(FString Answer)
{

	FBackendAnswer AnswerModel;

	if (FJsonObjectConverter::JsonObjectStringToUStruct(Answer, &AnswerModel))
	{
		if (!AnswerModel.Success)
		{
			if (AnswerModel.Error == "AccountAlreadyExist")
			{
				OnRegister.Broadcast(false, ERegisterError::AlreadyExist);
				return;
			}
			if (AnswerModel.Error == "EmailAlreadyExist")
			{
				OnRegister.Broadcast(false, ERegisterError::EmailAlreadyExist);
				return;
			}

		}
		else
		{
			OnRegister.Broadcast(true, ERegisterError::Unknown);

			USC_GameInstance* SCGI = Cast<USC_GameInstance>(GI);

			if (SCGI)
			{
				SCGI->SetUserSelfData(AnswerModel.Answer);
			}
			else
			{
				UE_LOG(LogRegisterController, Fatal, TEXT("ERROR: USC_RegistrerController::HandleAnswer(), Can't get SC_GameInstance"));
			}
			
		}
	}
}
