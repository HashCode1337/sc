// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_ViewControllerBase.h"
#include "SC_LoginController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogLoginController, Log, All);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLogged, bool, bSuccess, ELoginError, Error);

UCLASS()
class SC_API USC_LoginController : public USC_ViewControllerBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FOnLogged OnLogged;

	UFUNCTION(BlueprintCallable)
	void TryLogin(FString Login, FString Password);
	void SendRequest(FString Login, FString Password);

	virtual void HandleAnswer(FString Answer) override;

};
