// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SC : ModuleRules
{
	public SC(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Json", "JsonUtilities" });
		PrivateDependencyModuleNames.AddRange(new[] { "XmlParser", "ImageWrapper", "HTTP", "SlateCore" });
	}
}
