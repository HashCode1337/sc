// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"
#include "SC_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChanged, float, NewHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeath);


UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USC_HealthComponent();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true", ClampMin = "0", ClampMax = "10"))
	float MaxHealth = 10;
	
	UPROPERTY(ReplicatedUsing = OnRep_Health, BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true", ClampMin = "0", ClampMax = "1"))
	float Health = 1;
	UFUNCTION()
	void OnRep_Health();

	UFUNCTION(BlueprintNativeEvent)
	void HealthChanged(float NewHealth);

	UFUNCTION(BlueprintNativeEvent)
	void Death();

	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void AddDamage(float damage);

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FHealthChanged OnHealthChanged;

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FDeath OnDeath;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
