// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_HealthComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
USC_HealthComponent::USC_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);


}

void USC_HealthComponent::HealthChanged_Implementation(float NewHealth)
{
	
}

void USC_HealthComponent::AddDamage_Implementation(float damage)
{

	float dmgClamped = UKismetMathLibrary::Clamp(damage, 0, 1);
	float tempHealth = Health - damage;

	if (damage <= 0)
	{
		return;
	}
	
	if (tempHealth < 0)
	{
		Health = 0;
	}
	else
	{
		Health = tempHealth;
	}

	OnRep_Health();
}

void USC_HealthComponent::OnRep_Health()
{
	ACharacter* CompOwner = GetOwner<ACharacter>();

	if (!CompOwner) return;

	if (CompOwner->IsLocallyControlled())
	{
		FString msg = FString::Printf(TEXT("Local : NEW HEALTH %f"), Health);
		GEngine->AddOnScreenDebugMessage(1, 10, FColor::Green, msg);

		HealthChanged(Health);
		OnHealthChanged.Broadcast(Health);
	}
	else
	{
		FString msg = FString::Printf(TEXT("No Local : NEW HEATL %f"), Health);
		GEngine->AddOnScreenDebugMessage(2, 10, FColor::Yellow, msg);
	}

	if (Health <= 0)
	{
		OnDeath.Broadcast();
	}
}

void USC_HealthComponent::Death_Implementation()
{
}

// Called when the game starts
void USC_HealthComponent::BeginPlay()
{
	Super::BeginPlay();	
}



void USC_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*ACharacter* owner = GetOwner<ACharacter>();

	if (owner)
	{
		if (owner->IsLocallyControlled())
		{
			FString str = FString::Printf(TEXT("My Comp, %s"), *owner->GetName());
			GEngine->AddOnScreenDebugMessage(1, 11, FColor::Red, str);

			
		}
		else
		{
			FString str = FString::Printf(TEXT("Not My Comp, %s"), *owner->GetName());
			GEngine->AddOnScreenDebugMessage(2, 11, FColor::Red, str);
		}
		
	}*/

}

void USC_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(USC_HealthComponent, Health, COND_OwnerOnly);
}
