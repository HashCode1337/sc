// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SCGameMode.generated.h"

UCLASS(minimalapi)
class ASCGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASCGameMode();

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	

	/*virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void InitGameState() override;
	virtual TSubclassOf<AGameSession> GetGameSessionClass() const override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual int32 GetNumPlayers() override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual int32 GetNumSpectators() override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual void StartPlay() override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual bool HasMatchStarted() const override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual bool HasMatchEnded() const override;

	virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	virtual bool ClearPause() override;
	virtual bool AllowPausing(APlayerController* PC = nullptr) override;
	virtual bool IsPaused() const override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual void ResetLevel() override;

	UFUNCTION(BlueprintCallable, Category = Game)
	virtual void ReturnToMainMenuHost() override;

	virtual bool CanServerTravel(const FString& URL, bool bAbsolute) override;

	virtual void ProcessServerTravel(const FString& URL, bool bAbsolute = false) override;

	virtual void GetSeamlessTravelActorList(bool bToTransition, TArray<AActor*>& ActorList) override;


	virtual void SwapPlayerControllers(APlayerController* OldPC, APlayerController* NewPC) override;


	virtual TSubclassOf<APlayerController> GetPlayerControllerClassToSpawnForSeamlessTravel(APlayerController* PreviousPlayerController) override;

	virtual void HandleSeamlessTravelPlayer(AController*& C) override;

	virtual void PostSeamlessTravel() override;

	virtual void StartToLeaveMap() override;

	virtual void GameWelcomePlayer(UNetConnection* Connection, FString& RedirectURL) override;
	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	virtual APlayerController* Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	virtual void Logout(AController* Exiting) override;

	UFUNCTION(BlueprintCallable, Category = Game)
		virtual void ChangeName(AController* Controller, const FString& NewName, bool bNameChange) override;

	UFUNCTION(BlueprintCallable, Category = Game)
		virtual void RestartPlayer(AController* NewPlayer) override;

	UFUNCTION(BlueprintCallable, Category = Game)
		virtual void RestartPlayerAtPlayerStart(AController* NewPlayer, AActor* StartSpot) override;

	UFUNCTION(BlueprintCallable, Category = Game)
		virtual void RestartPlayerAtTransform(AController* NewPlayer, const FTransform& SpawnTransform) override;
	virtual void SetPlayerDefaults(APawn* PlayerPawn) override;
	virtual bool AllowCheats(APlayerController* P) override;

	virtual bool IsHandlingReplays() override;

	virtual bool SpawnPlayerFromSimulate(const FVector& NewLocation, const FRotator& NewRotation) override;

	virtual void PreInitializeComponents() override;
	virtual void Reset() override;*/

};



