// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_DataBase.h"
#include "../Core/SC_StaticLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetStringLibrary.h"

DEFINE_LOG_CATEGORY(LogDatabase);

USC_DataBase::USC_DataBase()
{
	ConnectToDataBase();
	CustomTick(0.f);

	ConnectionCheckTime = -CheckInterval + 0.5f;
}

USC_DataBase::~USC_DataBase()
{
	
}

float USC_DataBase::GetTime()
{
	return UKismetSystemLibrary::GetGameTimeInSeconds(this->GetOuter());
}

EMySQLConnectionState USC_DataBase::GetDataBaseConnectionState(FString& ErrorMsg)
{
	return UMySQLBPLibrary::CheckConnectionState(ErrorMsg);
}

void USC_DataBase::SetStateIfConnected()
{
	FString LogMsg;
	FString ErrorMsg;
	float CurrentTime = GetTime();

	if (CurrentTime > ConnectionCheckTime + CheckInterval)
	{
		EMySQLConnectionState State = GetDataBaseConnectionState(ErrorMsg);

		ConnectionCheckTime = GetTime();
		UE_LOG(LogDatabase, Log, TEXT("DB Connection check"));

		if (State == EMySQLConnectionState::Open && !Connected)
		{
			Connected = true;
			UE_LOG(LogDatabase, Log, TEXT("Connected To DataBase"));

		}
		else if (State == EMySQLConnectionState::Broken && ErrorMsg != LastErrorMsg)
		{
			LogMsg = "BD Connection Error" + ErrorMsg;
			LastErrorMsg = ErrorMsg;

			UE_LOG(LogDatabase, Fatal, TEXT("DB Error: %s"), *ErrorMsg);
		}
		else if (State == EMySQLConnectionState::Closed)
		{
			UE_LOG(LogDatabase, Warning, TEXT("Why connection is Closed???, Reopen"));
			Connected = false;
		}
	}
}

void USC_DataBase::ConnectToDataBase()
{
	FDataBaseSettings DataBaseSettings;
	USC_StaticLibrary::GetDataBaseSettings(DataBaseSettings);
	FString ExtraParams = "";

	UMySQLBPLibrary::SetConnectionProperties(DataBaseSettings.IP, DataBaseSettings.DataBaseName, DataBaseSettings.UserId, DataBaseSettings.Password, ExtraParams, false);

#pragma region TEST

	/*FString Request = FString::Printf(TEXT("SELECT CASE WHEN EXISTS(SELECT id FROM accounts WHERE account = '%s') THEN 1 ELSE 0 END"), TEXT("hashcode"));
	FString Request2 = FString::Printf(TEXT("SELECT CASE WHEN EXISTS(SELECT id FROM accounts WHERE account = '%s') THEN 1 ELSE 0 END"), TEXT("hashcode1"));
	FString Request3 = FString::Printf(TEXT("SELECT CASE WHEN EXISTS(SELECT id FROM accounts WHERE account = '%s') THEN 1 ELSE 0 END"), TEXT("hashcode2"));
	FString Request4 = FString::Printf(TEXT("SELECT CASE WHEN EXISTS(SELECT id FROM accounts WHERE account = '%s') THEN 1 ELSE 0 END"), TEXT("hashcode3"));
	FString Request5 = FString::Printf(TEXT("SELECT CASE WHEN EXISTS(SELECT id FROM accounts WHERE account = '%s') THEN 1 ELSE 0 END"), TEXT("hashcode4"));
	
	FString FullTakeReq = FString::Printf(TEXT("SELECT * FROM accounts WHERE account = '%s'"), TEXT("testing"));
	FString FullTakeReq2 = FString::Printf(TEXT("SELECT * FROM accounts WHERE account = '%s'"), TEXT("hashcode"));


	AddToQuery(Request);
	AddToQuery(Request2);
	AddToQuery(Request3);
	AddToQuery(Request4);
	AddToQuery(Request5);

	AddToQuery(FullTakeReq);
	AddToQuery(FullTakeReq2);*/

#pragma endregion

}

void USC_DataBase::CustomTick(float DeltaTime)
{
	SetStateIfConnected();
	TryExecQuery();
	TryGetAnswer();
}

int64 USC_DataBase::AddToQuery(FString Query)
{
	LastKey++;
	Queries.Add(Query);
	UE_LOG(LogDatabase, Log, TEXT("Added Query, Query ID:%i Query:%s"), CurrentKey, *Query);
	return LastKey;
}

void USC_DataBase::TryExecQuery()
{
	if (!Connected) return;

	if (Queries.Num() > 0 && CurrentQuery.IsEmpty())
	{
		Queries.HeapPop(CurrentQuery, true);
		UE_LOG(LogDatabase, Log, TEXT("Request, Query ID:%i Query:%s"), CurrentKey + 1, *CurrentQuery);
		UMySQLBPLibrary::SelectDataFromQueryAsync(CurrentQuery);
	}
}

void USC_DataBase::TryGetAnswer()
{
	if (!Connected) return;

	if (CurrentKey < LastKey)
	{
		FString ErrorMsg;
		EMySQLQueryExecutionState State;;


		State = UMySQLBPLibrary::CheckQueryExecutionState(ErrorMsg);

		switch (State)
		{

		case EMySQLQueryExecutionState::Executing:
		{

			break;
		}
		case EMySQLQueryExecutionState::Failed:
		{
			CurrentKey++;
			Busy = false;

			OnDataBaseAnswer.Broadcast(CurrentKey, false);
			UE_LOG(LogDatabase, Error, TEXT("AnswerKey:%i ERROR:%s"), CurrentKey, *ErrorMsg);

			break;
		}
		case EMySQLQueryExecutionState::Success:
		{
			TArray<FMySQLDataTable> AsDataTable;
			TArray<FMySQLDataRow> AsDataRow;

			UMySQLBPLibrary::GetSelectedTable(AsDataTable, AsDataRow);
	
			CurrentKey++;
			Busy = false;
#pragma region test

			UE_LOG(LogDatabase, Log, TEXT("**************************************************"));

			for (const FMySQLDataRow& row : AsDataRow)
			{
				//el.RowData
				for (const FString& str : row.RowData)
				{
					UE_LOG(LogDatabase, Log, TEXT("AnswerKey:%i Query:%s"), CurrentKey, *str);
				}
			}

			UE_LOG(LogDatabase, Log, TEXT("**************************************************"));

#pragma endregion
			FDataBaseAnswer Answer;
			Answer.ResultsByColumn = AsDataTable;
			Answer.ResultsByRow = AsDataRow;

			AnswersPool.Add(CurrentKey, Answer);
			OnDataBaseAnswer.Broadcast(CurrentKey, true);

			CurrentQuery.Empty(0);

			break;
		}

		}

	}
}

bool USC_DataBase::TryGetDataFromPool(int64 Key, FDataBaseAnswer& OutData)
{
	bool Result = false;

	if (AnswersPool.Contains(Key))
	{
		FDataBaseAnswer* Ans = AnswersPool.Find(Key);
		
		if (Ans->ResultsByColumn.Num() > 0)
		{
			if (Ans->ResultsByColumn[0].ColumnData.Num() > 0)
			{
				OutData = *Ans;
				Result = true;
			}
			else
			{
				Result = false;
			}
		}
		else
		{
			Result = false;
		}
	}

	return Result;
}
