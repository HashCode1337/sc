// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AnswerHandler.h"
#include "SC_DataBase.h"
#include "Kismet/KismetStringLibrary.h"
#include "Serialization/JsonWriter.h"
#include "JsonObjectConverter.h"
#include "../Core/SC_StaticLibrary.h"

DEFINE_LOG_CATEGORY(LogDatabaseAnswerHandler);

USC_AnswerHandler::USC_AnswerHandler()
{
	
}

USC_AnswerHandler::~USC_AnswerHandler()
{

}

void USC_AnswerHandler::PostInitProperties()
{
	Super::PostInitProperties();
}

void USC_AnswerHandler::Init()
{
	if (DataBase)
	{
		
		CurrentMode = USC_StaticLibrary::RecognizeMsg(IncomeMessage);

		switch (CurrentMode)
		{

		case ELoginType::Login:
		{
			FString Login;
			FString Pwd;

			USC_StaticLibrary::ParseCredentials(IncomeMessage, Login, Pwd);

			FString RgxLoginPattern = USC_StaticLibrary::GetNicknameRgxPattern();

			if (USC_StaticLibrary::RegexMatch(RgxLoginPattern, Login))
			{
				FString Query = FString::Printf(TEXT("SELECT * FROM accounts WHERE account = '%s'"), *Login);

				DataBase->OnDataBaseAnswer.AddDynamic(this, &USC_AnswerHandler::OnLoginCallback);
				DataBase->AddToQuery(Query);
			}

			

			break;
		}
		case ELoginType::Register:
		{

			break;
		}
		default:
		{

			break;
		}

		}
	}
}

void USC_AnswerHandler::OnLoginCallback(int64 Key, bool bSuccess)
{
	if (handlerExpired) return;

	FDataBaseAnswer Ans;
	if (bSuccess && !handlerExpired)
	{
		handlerExpired = true;

		if (DataBase->TryGetDataFromPool(Key, Ans))
		{

			TSharedRef<FJsonObject> AnswerAsJson = MakeShared<FJsonObject>();

			FString Login;
			FString Pwd;

			USC_StaticLibrary::ParseCredentials(IncomeMessage, Login, Pwd);

			// �������� Json ���� ����������� � ���������
			for (FMySQLDataTable dt : Ans.ResultsByColumn)
			{
				AnswerAsJson->SetStringField(dt.ColumnName, dt.ColumnData[0]);
			}

			FDataBaseAccountModel AnswerModel;
			bool Converted = FJsonObjectConverter::JsonObjectToUStruct(AnswerAsJson, &AnswerModel);

			if (Converted)
			{
				//USC_StaticLibrary::PrintMsg(0, FColor::Blue, AnswerModel.password);
				bool pwdMatch = AnswerModel.password == Pwd;

				CallBackHolder.OnLogin.ExecuteIfBound(ClientSocketId, pwdMatch);
				MarkHandlerAsExpired();
			}
			else
			{
				CallBackHolder.OnLogin.ExecuteIfBound(ClientSocketId, false);
				MarkHandlerAsExpired();
				UE_LOG(LogDatabaseAnswerHandler, Error, TEXT("ERROR: USC_AnswerHandler::OnLoginCallback(), cant parse json to struct"));
			}
		}
		else
		{
			UE_LOG(LogDatabase, Error, TEXT("ERROR: USC_AnswerHandler::OnLoginCallback() was success but data not valid WHAT A FUCK?"));
			CallBackHolder.OnLogin.ExecuteIfBound(ClientSocketId, false);
			MarkHandlerAsExpired();
		}
	}
	else
	{
		CallBackHolder.OnLogin.ExecuteIfBound(ClientSocketId, false);
		MarkHandlerAsExpired();
	}
	
}

void USC_AnswerHandler::MarkHandlerAsExpired()
{
	handlerExpired = true;
	ConditionalBeginDestroy();
}
