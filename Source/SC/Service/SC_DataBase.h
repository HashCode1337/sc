// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MySQLBPLibrary.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_DataBase.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogDatabase, Log, All);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAnswer, int64, Key, bool, bSuccess);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDetailedAnswer, EAnswerType, AnswerType, int64, Key, const FDataBaseAnswer&, DetailedAnswer);

UCLASS(Blueprintable)
class SC_API USC_DataBase : public UObject
{
private:

	USC_DataBase();
	~USC_DataBase();

	GENERATED_BODY()

	UPROPERTY()
	TMap<int64, FDataBaseAnswer> AnswersPool;
	UPROPERTY()
	int64 LastKey = -1;

	UPROPERTY()
	int64 CurrentKey = -1;
	
	UPROPERTY()
	bool Connected = false;
	UPROPERTY()
	bool Busy = false;
	UPROPERTY()
	float ConnectionCheckTime = 0.0f;
	UPROPERTY()
	float CheckInterval = 5.0f;
	UPROPERTY()
	FString LastErrorMsg;


	UPROPERTY()
	TArray<FString> Queries;
	UPROPERTY()
	FString CurrentQuery;


public:

	//===============================================================================//

	UPROPERTY(BlueprintAssignable, Category = "SC | DB | Answer")
	FOnAnswer OnDataBaseAnswer;

	//===============================================================================//
	
	UFUNCTION()
	float GetTime();

	UFUNCTION(BlueprintCallable)
	EMySQLConnectionState GetDataBaseConnectionState(FString& ErrorMsg);

	UFUNCTION(BlueprintCallable)
	void SetStateIfConnected();

	UFUNCTION(BlueprintCallable)
	void ConnectToDataBase();

	UFUNCTION(BlueprintCallable)
	void CustomTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	int64 AddToQuery(FString Query);
	
	UFUNCTION(BlueprintCallable)
	void TryExecQuery();

	UFUNCTION(BlueprintCallable)
	void TryGetAnswer();

	UFUNCTION(BlueprintCallable)
	bool TryGetDataFromPool(int64 Key, FDataBaseAnswer& OutData);
};