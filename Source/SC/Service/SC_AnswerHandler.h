// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_AnswerHandler.generated.h"

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnCredentials, int64, ClientId, bool, bIsMatch);
DECLARE_LOG_CATEGORY_EXTERN(LogDatabaseAnswerHandler, Log, All);


USTRUCT(BlueprintType)
struct FDelegateHolder
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FOnCredentials OnLogin;




};

class USC_DataBase;

UCLASS(Blueprintable)
class SC_API USC_AnswerHandler : public UObject
{
	GENERATED_BODY()
private:
	USC_AnswerHandler();
	~USC_AnswerHandler();
	virtual void PostInitProperties() override;

	int64 RequestKey = -1;
	ELoginType CurrentMode;
	bool handlerExpired = false;

public:

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	int64 ClientSocketId;

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	FString IncomeMessage;

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	USC_DataBase* DataBase;

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	FDelegateHolder CallBackHolder;

	UFUNCTION(BlueprintCallable)
	void Init();

private:

	//===============================================================================//

	UFUNCTION()
	void OnLoginCallback(int64 Key, bool bSuccess);

	UFUNCTION()
	void MarkHandlerAsExpired();


};
