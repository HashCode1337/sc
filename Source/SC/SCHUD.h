// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SCHUD.generated.h"

class UWidgetBlueprint;

UCLASS()
class ASCHUD : public AHUD
{
	GENERATED_BODY()

public:
	
	ASCHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintNativeEvent)
	void SetupMainHUD();

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

